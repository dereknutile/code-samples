$(document).ready(function() {

    // ------------ START FUNCTIONS --------------- //

    function clearItem(attribute_id) {
        var item_iterator = attribute_id.substring(9);
        $("#item_sku_"+item_iterator).val("");
        $("#item_qty_"+item_iterator).val("");
        $("#item_name_"+item_iterator).html("");
        // $("#item_price_"+item_iterator).html("");
        $("#item_price_"+item_iterator).val("");
        $("#item_price_total_"+item_iterator).html("");
    }


    function calculate() {
        var grand_total = 0;
        $("input.item_qtys").each(function(){
            var quantity;
            var price;
            var subtotal;
            var current_id = $(this).attr('id');
            var item_iterator = current_id.substring(9);

            quantity = this.value == "" ? null : parseInt(this.value);

            if(quantity > 0){
                // price = $("#item_price_"+item_iterator).text();
                price = $("#item_price_"+item_iterator).val();
                subtotal = price * quantity;
                grand_total += subtotal;
            }
        });
        var formatted_grand_total = grand_total.toFixed(2);
        $("#grand_total").html("$"+formatted_grand_total);
    }

    function hideRows(minHideNumber){
        var startRow = minHideNumber;
        var rowCount = $('#express-order-table tr').length;
        var endRow = rowCount -2;

        for (i=startRow; i<=endRow; i++){
            $('#item_row_'+i).hide();
        }
    }

    function rowUpdate(rowNumber, forcePrice){
        var oetc_sku = $("#item_sku_"+rowNumber).val();
        var oetc_qty = $("#item_qty_"+rowNumber).val();
        var oetc_price = $("#item_price_"+rowNumber).val();

        if(oetc_sku.length > 0){
            $("#item_sku_"+rowNumber).val(oetc_sku.toUpperCase());
            if(oetc_qty.length == 0){
                oetc_qty = 1;
                $("#item_qty_"+rowNumber).val(oetc_qty);
            }

             $.post("../webadmin/get_item_json", {"oetc_sku" : oetc_sku},
                function(data){
                item_name = data.item_name;

                if(forcePrice == true){
                    oetc_price = data.item_price;
                }

                if(oetc_price.length === 0){
                    item_price = data.item_price;
                } else {
                    item_price = parseFloat(oetc_price).toFixed(2);
                }
                item_type = data.item_type;
                subtotal = item_price * oetc_qty;
                subtotal = subtotal.toFixed(2);
                item_price = parseFloat(item_price).toFixed(2);

                $("#item_name_"+rowNumber).removeClass("discontinued");
                $("#item_name_"+rowNumber).removeClass("missing");

                if(item_type == 2){
                    $("#item_name_"+rowNumber).addClass("discontinued");
                }

                if(item_type == 0) {
                    $("#item_name_"+rowNumber).addClass("missing");
                }

                $("#item_name_"+rowNumber).html(item_name);
                $("#item_price_"+rowNumber).val(item_price);
                $("#item_price_total_"+rowNumber).html(subtotal);
                }, "json");
        }
        calculate();
    }


    function memberIdChange(){
        var member_id = $('#member_id').val();
        var member_group = $('#member_group').val();
        $('#member_id').val(member_id.toUpperCase());

         $.post("../webadmin/get_member_json", {"member_id":member_id, "member_group":member_group},
            function(data){
                member_name = data.name;
                if(member_name == 'Member not found'){
                    $("#member_name_output").html(member_name).show();
                } else {
                    $("#member_name_output").html("Ship To: " + member_name).show();
                }

                ia_emails = data.ia_emails;
                if(ia_emails){
                    $("#order_recipients").html("<b>Send Email Confirmations To:</b><br />");
                    for(mail in ia_emails)
                    {
                        var email_checkbox = 'email_check_'+mail;
                        var checked = '';
                        if(ia_emails[mail].value == "1"){
                            checked = ' checked="checked"';
                        }
                        var email_line = '<div><input type="checkbox" name="'+email_checkbox+'" value="'+ia_emails[mail].email+'""'+checked+'" />&nbsp;'+ia_emails[mail].firstname+' '+ia_emails[mail].lastname+' ('+ia_emails[mail].name+')</div>';
                        $("#order_recipients").append(email_line);
                    }
                } else {
                    $("#order_recipients").html("<b>Send Email Confirmations To:</b><p>No Institutional Administrators or Authorized Purchasers exist for this insitution or group.</p>");
                }

               recent_pos = data.recent_pos;
               // var recent_pos = "Recent PO's Go Here!";
               if(recent_pos){
                   $(".recent_pos").show();
                   $("#recent_pos").html(recent_pos).show();
               }

                if(data.contact_name.length > 0){
                    $('#member_shipping').show();
                    // $("#member_shipping_error").hide();

                    $('#shipping_fullname').val(data.contact_name);
                    // var full_shipping_address = data.contact_name;
                    if(data.address1 && data.address1.length > 0){
                        // full_shipping_address += "<br />"+data.address1;
                        $('#shipping_address_1').val(data.address1);
                    }
                    if(data.address2 && data.address2.length > 0){
                        // full_shipping_address += "<br />"+data.address2;
                        $('#shipping_address_2').val(data.address2);
                    }
                    if(data.address3 && data.address3.length > 0){
                        // full_shipping_address += "<br />"+data.address3;
                        $('#shipping_address_3').val(data.address3);
                    }
                    if(data.city.length > 0){
                        // full_shipping_address += "<br />"+data.city;
                        $('#shipping_city').val(data.city);
                    }
                    if(data.state.length > 0){
                        // full_shipping_address += "<br />"+data.state;
                        $('#shipping_state').val(data.state);
                    }
                    if(data.zipcode.length > 0){
                        // full_shipping_address += "<br />"+data.zipcode;
                        $('#shipping_zipcode').val(data.zipcode);
                    }

                    $('#member-details').show();
                    $('#cart').show(500);
                    // $("#member_shipping").append("<b>Shipping Address:</b><br />"+full_shipping_address);
                } else {
                    $('#member_shipping').hide();
                    $('#member-details').hide();
                    $('#cart').hide();
                }
            }, "json");
    }

    // ------------ END FUNCTIONS --------------- //

    var initialRowDisplay = 6;  // Note: this includes the row with the 'add' button, so 6 is 5 express rows plus one for the button
    var incrementRowDisplay = 1;
    var totalRows = $('#express-order-table').find('tr').length - 2;

    $('#hidden_member_id').hide();
    $('#member_name_output').hide();
    $('#member_groups_output').hide();
    $('#member-details').hide();
    $('#cart').hide();
    hideRows(initialRowDisplay); // Hide all entry rows >= this value

    //attach autocomplete
    $("#member_peek").autocomplete({
        highlight: false,
        scroll: true,
        minLength: 3,
        // delay: 600,
        //define callback to format results
        source: function(req, add){

            //pass request to server
            $.getJSON("../webadmin/get_member_autocomplete_json/", req, function(data) {

                //create array for the members
                var members = [];
                //process response
                $.each(data, function(i, val) {
                    // members.push(val.name);
                    members.push(val.id + " - " + val.name);
                });

            //pass array to callback
            add(members);
        });
    },
        //define select handler
        // select: function(e, ui) {},

        //define select handler
        change: function(e, name) {
            var member_name = name.item.value;

            // grab the member id from the string MEMBERID - Member name
            var member_id = member_name.split(' - ', 1);
            var member_len = member_name.length -1;
            $("#member_id").val(member_id);

            var group_start = member_name.indexOf("Group: ");
            var group_name = "";
            if(group_start > 0)
            {
                group_start = group_start + 7;
                group_name = member_name.substr(group_start, member_len - group_start);
            }
            $("#member_group").val(group_name);
            $(".member_name_bar").hide();
            memberIdChange();
        }
    });



    $('#member_name_input').click(function(){
        // $('#member_name_output').hide();
        $('#member_name_input').show();
        $('input[name=member_peek]').select();
    });

    $("#add-row").click(function() {
        initialRowDisplay = initialRowDisplay + incrementRowDisplay;
        if (initialRowDisplay >= totalRows){
            $('#add-row').remove();
        }

        if (totalRows - initialRowDisplay > incrementRowDisplay){
            $('table').find('tr:lt('+initialRowDisplay+')').show(600);
        }
        return false;
    });

    $("input.item_skus").blur(function(){
        var current_sku = $(this).attr('id');
        var rowNumber = current_sku.substring(9);
        if($(this).val().length == 0){
            clearItem($(this).attr('id'));
        }
        rowUpdate(rowNumber, true);
    });

    $("input.item_qtys").keypress(function(){
        if(event.charCode && (event.charCode <47 || event.charCode > 57))
        {
            event.preventDefault();
        }
    });

    $("td input.item_qtys").blur(function(){
        var current_id = $(this).attr('id');
        var rowNumber = current_id.substring(9);
        if($("input.item_qtys").val() == "0" || $("input.item_qtys").length == 0){
            clearItem($(this).attr('id'));
        }
        rowUpdate(rowNumber);
    });

    $("input.item_prices").blur(function(){
        var current_id = $(this).attr('id');
        var rowNumber = current_id.substring(11);
        rowUpdate(rowNumber);
    });
});