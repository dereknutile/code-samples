@echo OFF
cls

set backroot=D:\arc\moodles
set zip=D:\bin\gzip\bin\gzip.exe

for /f "tokens=1-4 delims=/ " %%a in ('date/t') do (
set dw=%%a
set mm=%%b
set dd=%%c
set yy=%%d
)
:: Create the filename suffix
set today=%yy%_%mm%_%dd%
set backdir=%backroot%\%today%

mkdir %backdir%

for /f %%i in ('mysql --host=localhost --user=username -p"password" -Bse "show databases"') do (
mysqldump --opt --host=localhost --user=username -p"password" %%i --single-transaction -R  | %zip% > %backroot%\%%i%today%.zip
)