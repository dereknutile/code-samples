#!/usr/bin/env sh
# automation script to make sure Nodejs is up-to-date

echo Current NodeJS version
node -v

echo Cleaning the NodeJS cache ...
sudo npm cache clean -f

echo Install/update the N module ...
## see: https://github.com/visionmedia/n
sudo npm install -g n

echo Upgrading NodeJs to the stable official release ...
sudo n stable

echo Current NodeJS version
node -v