"""
Writing custom django-admin commands
see: https://docs.djangoproject.com/en/1.6/howto/custom-management-commands/

This script polls the apps.folder.Downloads table to see if there are processing
downloads.  If a processing download zip is found in AWS S3, that download is set
to complete.

"""

import subprocess
# include `import subprocess` to work shell commands
# see: https://docs.python.org/2/library/subprocess.html
import boto
from datetime import datetime, timedelta
from django.conf import settings
from django.core.mail import send_mail
from django.utils.timezone import utc
from django.core.management.base import BaseCommand, CommandError
from apps.folder.models import Download, DownloadFile


class Command(BaseCommand):
    args = '<noargs ...>'
    help = 'Queries for and processes any downloads marked processing.'

    def handle(self, *args, **options):
        # poll the database for downloads that are in 'processing mode' and are
        # older than 24 hours - these are flagged for error
        twentyFourHoursAgo = datetime.utcnow().replace(tzinfo=utc)-timedelta(days=1)
        erroredDownloads = Download.objects.filter(status=2).filter(created_at__lte=twentyFourHoursAgo)

        if erroredDownloads:
            for erroredDownload in erroredDownloads:
                self.setDownloadToError(erroredDownload.id)

        # poll the database for downloads that are in 'processing mode'
        processingDownloads = Download.objects.filter(status=2)

        if processingDownloads:
            s3 = boto.connect_s3()

            for processingDownload in processingDownloads:
                # we check AWS S3 for the zip file, if it's there, ZZTOP did her
                # job, if not, we pass ...
                key = s3.get_bucket(settings.AWS_STORAGE_BUCKET_NAME).get_key('zipfiles/'+processingDownload.name)
                if key:
                    self.setDownloadToComplete(processingDownload.id)
                    zipFileBaseUrl = settings.ZIP_FILE_BASE_URL

                    send_mail(
                        'Backstage Zipfile Ready',
                        'Your requested collection of assets has been zipped into the file: '+processingDownload.name+'. You can access this download from the Backstage My Downloads section here: '+zipFileBaseUrl,
                        'backstage.technical@converse.com',
                        [processingDownload.user.email],
                        fail_silently=True,
                    )
        pass

    def setDownloadToComplete(self,id):
        download = Download.objects.get(pk=id)
        download.status = 3
        download.save()

        return True


    def setDownloadToError(self,id):
        download = Download.objects.get(pk=id)
        download.status = 4
        download.save()

        return True
