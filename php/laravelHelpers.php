<?php

/**
 *
 * Simple library of PHP helpers.  This has been imported from a collection
 * of helpers built and used in CodeIgniter over the years.
 *
**/

class Helpers
{
    // -------------------------------------------------------------------------
    // HTML Helpers
    // -------------------------------------------------------------------------


    /**
     *
     * HTML form helper to disable element
     * check the value, if non-affirmative, return disabled="disabled"
     *
     * @access public
     * @param mixed $value
     * @return string
     */
    public static function set_disabled ($value = null)
    {
        $return = '';

        if(isset($value))
        {
            switch ($value)
            {
                case 0:
                    $return = 'disabled="disabled"';
                    break;
                case "0":
                    $return = 'disabled="disabled"';
                    break;
                case false:
                    $return = 'disabled="disabled"';
                    break;
            }
        }

        return $return;
    }


    /**
     *
     * HTML form helper to set an element as checked
     * check the value, if non-affirmative, return checked="checked"
     *
     * @access public
     * @param mixed $value
     * @return string
     */
    public static function set_checked ($value = null)
    {
        $return = '';

        if(isset($value))
        {
            switch ($value)
            {
                case 1:
                    $return = 'checked="checked"';
                    break;
                case "1":
                    $return = 'checked="checked"';
                    break;
                case true:
                    $return = 'checked="checked"';
                    break;
            }
        }

        return $return;
    }


    /**
     *
     * HTML form helper used to set input value to true
     * check the value, if affirmative, return true
     *
     * @access public
     * @param mixed $value
     * @return string
     */
    public static function set_t_or_f ($value = null)
    {
        $return = false;

        if(isset($value))
        {
            switch ($value)
            {
                case 1:
                    $return = true;
                    break;
                case "1":
                    $return = true;
                    break;
            }
        }

        return $return;
    }


    /**
     *
     * HTML helper used to turn a 0 into a NO or a 1 into a Yes, etc.
     *
     * @param integer $integer
     * @param string $case tells the helper which case to return the output, use
     *          PHP strtolower strtoupper ucwords
     * @return string
     *
     */
    public static function yes_or_no ($integer = null, $case = 'ucfirst')
    {
        $return = 'unknown';

        if(isset($integer))
        {
            switch ($integer)
            {
                case 0:
                    $return = 'no';
                    break;
                case 1:
                    $return = 'yes';
                    break;
            }
        }

        if($case)
        {
            switch ($case)
            {
                case 'ucfirst':
                    $return = ucfirst($return);
                    break;
                case 'ucwords':
                    $return = ucwords($return);
                    break;
                case 'strtolower':
                    $return = strtolower($return);
                    break;
                case 'strtoupper':
                    $return = strtoupper($return);
                    break;
            }
        }

        return $return;
    }


    /**
     *
     * take a null value and replace it with text
     * used for outputting query results
     *
     * @access public
     * @param mixed $value
     * @return string
     */
    public static function null_to_text ($value = null, $output = 'n/a')
    {
        $return = $value;

        if($value == null)
        {
            $return = $output;
        }

        return $return;
    }


    /**
     *
     * if the $input is null, false, 0 chars long, it will return the $output,
     * otherwise, it will just pass the input string
     * good to replace blank table cells with more meaningful text
     *
     * @access public
     * @param string $input garbage in
     * @param string $output garbage out
     * @return string
     *
     */
    public static function blank_to_text ($input = null, $output = "blank")
    {
        $return = null;

        if($output)
        {
            $return = $input;

            if($input == null)
            {
                $return = $output;
            }

            if($input == false)
            {
                $return = $output;
            }

            if(strlen($output) < 1)
            {
                $return = $output;
            }
        }

        return $return;
    }


    /**
     *
     * gets a list of years relative to the current year
     *
     * @access public
     * @param integer $before
     * @param integer $after
     * @param integer $current optional way to change the base year
     * @param string $prefix prefixes the year - like FY2012
     * @return string
     */
    public static function datatables_script ($id, $source, $sort_col = 1, $sort_dir = 'desc', $length = 50)
    {
        /**
         * example
         *
            $(document).ready(function() {
                $('#data-table').dataTable( {
                    "bProcessing": true,
                    "bServerSide": true,
                    "sAjaxSource": "../controller/method",
                    "aaSorting": [[3,'desc']],
                    "iDisplayLength": 50,
                    "fnServerData": function ( sSource, aoData, fnCallback ) {
                        $.ajax({
                            "dataType": 'json',
                            "type": "POST",
                            "url": sSource,
                            "data": aoData,
                            "success": fnCallback
                        });
                    }
                });
            });
         */


        $return  = '<script type="text/javascript" charset="utf-8">'."\n";
        $return .= '$(document).ready(function() {'."\n";
        $return .= '$("#'.$id.'").dataTable( {'."\n";
        $return .= '"bProcessing": true,'."\n";
        $return .= '"bServerSide": true,'."\n";
        $return .= '"sAjaxSource": "'.$source.'",'."\n";
        $return .= '"aaSorting": [['.$sort_col.',"'.$sort_dir.'"]],'."\n";
        $return .= '"iDisplayLength": '.$length.','."\n";
        $return .= '"fnServerData": function ( sSource, aoData, fnCallback ) {
                        $.ajax( {
                            "dataType": "json",
                            "type": "POST",
                            "url": sSource,
                            "data": aoData,
                            "success": fnCallback
                        });
                    }'."\n";
        $return .= '});'."\n"; // end id.dataTable...
        $return .= "});"."\n"; // end $(document)...
        $return .= '</script>'."\n";

        return $return;
    }


    /**
     *
     * generates an icon formatted for Twitter Bootstrap/FontAwesome
     * see: http://twitter.github.com/bootstrap/base-css.html#icons
     *
     * @access public
     * @param string $icon_suffix like search for 'icon-search'
     * @param string $color_suffix like white for 'icon-white'
     * @return string
     */
    public static function tbui_icon ($icon_suffix = null, $color_suffix = null)
    {
        $return = null;

        if($icon_suffix)
        {
            $prefix = 'icon-';
            $color = '';

            $icon = $prefix.$icon_suffix;

            if($color_suffix)
            {
                $color = ' '.$prefix.$color_suffix;
            }

            $return = '<i class="'.$icon.$color.'"></i> ';
        }

        return $return;
    }


    // -------------------------------------------------------------------------
    // Date and Time Helpers
    // -------------------------------------------------------------------------


    /**
     *
     * gets the current timestamp and converts the components into an array
     *
     * @access public
     * @param string $date in database format (2012-03-23 15:12:59)
     * @return array
     */
    public static function date_to_array ($date = null)
    {
        $return = null;

        if($date)
        {
            $date_array = array();

            $date_array['year']   = substr($date, 0, 4);
            $date_array['month']  = substr($date, 5, 2);
            $date_array['day']    = substr($date, 8, 2);
            $date_array['hour']   = substr($date, 11, 2);
            $date_array['minute'] = substr($date, 14, 2);
            $date_array['second'] = substr($date, 17, 2);

            $return = $date_array;
        }

        return $return;
    }

    /**
     *
     * gets the current timestamp in common sql format
     *
     * @access public
     * @param string $format see: http://php.net/manual/en/function.date.php
     * @return string
     */
    public static function get_now ($format = 'Y-m-d G:i:s')
    {
        return date($format);
    }


    /**
     *
     * make the date pretty
     *
     * @access public
     * @param string $date formatted as 2013-04-30 09:28:17 and if not submitted
     *                     uses the current date/timestamp
     * @param string $format see: http://php.net/manual/en/function.date.php
     * @return string
     */
    public static function pretty_date ($date = null, $format = 'l, F j, Y')
    {
        $return = null;

        if($date == null)
        {
            $date = get_now();
        }

        $date = strtotime($date);

        $return = date($format, $date);

        return $return;
    }

    /**
     *
     * gets a list of months
     *
     * @access public
     * @return array
     */
    public static function get_months ()
    {
        $months = array(
            '01' => '01 - January',
            '02' => '02 - February',
            '03' => '03 - March',
            '04' => '04 - April',
            '05' => '05 - May',
            '06' => '06 - June',
            '07' => '07 - July',
            '08' => '08 - August',
            '09' => '09 - September',
            '10' => '10 - October',
            '11' => '11 - November',
            '12' => '12 - December',
        );
        return $months;
    }


    /**
     *
     * gets a list of years relative to the current year
     *
     * @access public
     * @param integer $before
     * @param integer $after
     * @param integer $current optional way to change the base year
     * @param string $prefix prefixes the year - like FY2012
     * @return array
     */
    public static function get_years ($before = 5, $after = 5, $base = null, $prefix = '')
    {
        $years = array();

        if($base)
        {
            $base_year = $base;
        } else {
            $base_year = get_now('Y');
        }

        $start_year = $base_year - $before;
        $end_year = $base_year + $after;
        $year = $start_year;

        while($year <= $end_year)
        {
            $years[$year] = $prefix.$year;
            $year++;
        }

        return $years;
    }


    // -------------------------------------------------------------------------
    // Miscellaneous Helpers
    // -------------------------------------------------------------------------


    /**
     *
     * formats an integer in a standard money format using number_format
     * see: http://www.php.net/manual/en/function.number-format.php
     *
     * @access public
     * @param integer $number
     * @param integer $decimals
     * @param string $symbol
     * @param string $point
     * @param string $thousands
     * @return array
     *
     */
    public static function monify ($number = null, $decimals = 2, $symbol = '$&nbsp;', $point = '.', $thousands = ',')
    {
        $return = null;

        if($number !== null)
        {
            $return = $symbol . number_format($number, $decimals, $point, $thousands);
        }

        return $return;
    }


    // -------------------------------------------------------------------------
    // Debug Helpers
    // -------------------------------------------------------------------------


    /**
     *
     * Simple library of PHP helpers.  This has been imported from a collection
     * of helpers built and used in CodeIgniter over the years.
     *
     * @access public
     * @param mixed $value
     * @return string
     */
    public static function dmp ()
    {
        list($back_trace) = debug_backtrace();
        $arguments = func_get_args();
        $total_arguments = count($arguments);

        if($total_arguments > 0)
        {
            $i = 1;

            echo '<div style="border: 2px dashed #b94a48; padding:12px; margin:20px;">';
            echo '<h4 style="color: #b94a48;">Debug Helper dmp()</h4>';
            echo "File: <strong>".$back_trace['file']."</strong><br />";
            echo "Line: <strong>".$back_trace['line']."</strong><br />";
            echo "Arguments: <strong>".$total_arguments."</strong><br />";
            echo "<hr />";

            foreach ($arguments as $argument)
            {
                echo '<strong>#'.$i.'</strong><br />';
                echo '<pre>';
                var_dump($argument);
                echo '</pre>';

                if($i < $total_arguments)
                {
                    echo '<hr />';
                }

                $i++;
            }

            echo '</div>';
        } else {
            echo "Nothing to debug.";
        }
    }


    /**
     * generate a random string
     * good for passwords or hashes
     *
     * @access public
     * @param integer $length of the result
     * @param boolean $hash the result t/f?
     * @return string
     */
    public static function generate_random_string ($length = 6, $hash = false)
    {
        $return = false;

        if($length)
        {
            $string = '';
            $p = 0;
            $allowed = '_0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz';
            $character = '';

            for ($p = 0; $p <= $length; $p++)
            {
                $character = mt_rand(0, strlen($allowed));
                $string .= substr($allowed, $character, 1);
            }

            if($hash == true)
            {
                $string = md5($string);
            }

            $return = $string;
        }

        return $return;
    }
}

/* End of file application_helper.php */
/* Location: ./application/helpers/application_helper.php */
