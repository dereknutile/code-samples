<?php
/**
 * Codeigniter cron scripts
**/

class Cron extends MY_Controller
{
    /**
     * -------------------------------------------------------------------------
     * tables
     * -------------------------------------------------------------------------
    **/
    private $table_cron_log = 'log_cron';


    /**
     * -------------------------------------------------------------------------
     * private vars
     * -------------------------------------------------------------------------
    **/
    private $bounceUrl = '/'; 


    /**
     * -------------------------------------------------------------------------
     * default constructor
     * -------------------------------------------------------------------------
    **/
    public function __construct ()
    {
        parent::__construct();
        $this->load->library('user_agent');
		$this->output->cache(0);

        $access = false;
        $accessType = 'Unknown';
        $remoteIp = $this->input->server('remote_addr');

        if(strlen(trim($remoteIp)) == 0)
        {
            $remoteIp = 'Unknown';
        }

        if(!$this->agent->is_robot())
        {
            if($this->input->is_cli_request())
            {
                $access = true;
                $accessType = 'Server Cron Job';
                $userAgent = 'CLI';
            } else {
                $access = true;
                $accessType = 'Browser Cron Job';
                $userAgent = $this->agent->agent_string();
            }
            // Check to see if this authenticated user can access this feature
            if ($access == true)
            {
                $data['type'] = 'Cron Executed';
                $data['details'] = "Cron successfully accessed by: ".$accessType.". IP: ".$remoteIp." User Agent: ".$userAgent;
                $this->logCron($data);
                /**
                * Turned off: 2013-02-22 in an attempt to lessen the size of the cron log
                *
                **/
            } else {
                $data['type'] = 'Cron Blocked';
                $data['details'] = "Cron access blocked. IP: ".$remoteIp." User Agent: ".$userAgent;
                $this->logCron($data);

                // The agent is a browser, but it is not using an admin context
                redirect($this->bounceUrl);
            }
        } else {
            $data['type'] = 'Cron Robot';
            $data['details'] = "Cron accessed by robot";
            $this->logCron($data);

            redirect($this->bounceUrl);
        }
    }


    /**
     * -------------------------------------------------------------------------
     * pseudo index
     * -------------------------------------------------------------------------
     *
     * @access public
     * @return void
    **/
	public function index ()
	{
        // no direct access to this script
        redirect($this->bounceUrl);
    }


    /**
     * -------------------------------------------------------------------------
     * process email queue
     * -------------------------------------------------------------------------
     *
     * @access public
     * @return void
    **/
    public function process_queue ()
    {
        $this->email_model->process_queue();
        if($this->email_model->result)
        {
            $result = $this->email_model->result;
        } else {
            $result = 'Email queue processed';
        }

        $data['type'] = 'Queue';
        $data['details'] = $result;
        $this->logCron($data);
    }


    /**
     * -------------------------------------------------------------------------
     * prune sessions table of specific data
     * -------------------------------------------------------------------------
     *
     * @access public
     * @return void
    **/
    public function purge_sessions ()
    {
        $this->load->model('webadmin/admin_model');
        $rows = $this->admin_model->purge_sessions();

        $data['type'] = 'Session purge';
        $data['details'] = 'Bot sessions purged: '.$rows;
        $this->logCron($data);
    }


    /**
     * -------------------------------------------------------------------------
     * log actions to the cron log
     * -------------------------------------------------------------------------
     *
     * @access private
     * @return boolean
	 */
	private function logCron ($data = null)
	{
        $return = false;

        if($data)
        {
            $log['type'] = $data['type'];
            $log['details'] = $data['details'];
            $log['created'] = get_now();
            $query = $this->db->insert($this->table_cron_log, $log);

            if ($query)
            {
                $return = true;
            }
        }

        return $return;
    }
}

/* End of cron controller */
/* Location: application/modules/cron/controllers/cron.php */
