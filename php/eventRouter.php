<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * automagic router for all events on the events.oetc.org site
 * 
 * this router allows the admin to create infinite events while keeping the 
 * URL clean
 *
**/

class Eventrouter extends Public_Controller
{
    /**
     * -------------------------------------------------------------------------
     * module variables
     * -------------------------------------------------------------------------
    **/
    private $module_class    = '';   // sets the class
    private $module_config   = null; // reads the module config/config.php file
    private $module_name     = '';   // module name
    private $module_navroot  = '';   // sets nav classes to active
    private $module_viewpath = '';   // path to view files


    /**
     * -------------------------------------------------------------------------
     * private variables
     * -------------------------------------------------------------------------
    **/
    private $addCrumb = array();
    private $pageHtml = null;
    private $eventData = null;
    private $message = '';
    private $page = null;
    private $params = null;


    /**
     * -------------------------------------------------------------------------
     * default constructor
     * -------------------------------------------------------------------------
    **/
	public function __construct ()
	{
        parent::__construct();
        $this->page = $this->uri->segment(1);
        $this->tpl_slug = $this->page; // set this in the global MY_Controller

        $this->load->model('eventrouter_model');

        $this->tpl_attendee_101_nav = $this->eventrouter_model->setNavigationByContext($this->page, 'attendee_101');
        $this->tpl_travel_nav = $this->eventrouter_model->setNavigationByContext($this->page, 'travel_html');
        $this->eventrouter_model->slug = $this->page;

        $this->reroute();
	}



    /**
     * -------------------------------------------------------------------------
     * pre router _remap class
     * -------------------------------------------------------------------------
     * See: http://ellislab.com/codeigniter/user-guide/general/controllers.html
     *
     * @access public
     * @return void
    **/
	public function _remap ($page, $params = array() )
	{
        // load any parameters into $this->params
		if(count($params) > 0)
		{
			if(strlen($params[0]) > 0)
			{
				$this->params = $params;
			}
		}

		if($this->params)
		{
            // parse out the first parameter, it's the method
			$method = strtolower(trim($this->params[0]));

		    if(method_exists($this, $method))
		    {
		        return call_user_func_array (array($this, $method), $this->params);
		    } else {
                $this->noroute();
				// $this->index();
		    }
		} else {
			$this->index();
		}
	}


    /**
     * -------------------------------------------------------------------------
     * prepare the view for rendering
     * -------------------------------------------------------------------------
     *
     * @access public
     * @return view $this->render
    **/
	public function index ()
	{
        $this->global_active_nav = 'overview';
        $this->eventrouter_model->slug = $this->page;

		if($this->eventrouter_model->testEventUrlSegment() == true)
		{
            $this->eventIndex();
		} else {
			echo $this->noroute();
		}
	}


    /**
     * -------------------------------------------------------------------------
     * set event variables
     * -------------------------------------------------------------------------
     *
     * @access private
     * @param string $context is the page we are navigating, like bio or index
     * @return void
    **/
    private function setEvent ($context = null)
    {
        if($this->page)
        {
            if(isset($this->global_user[0]->id))
            {
                $currentUserId = $this->global_user[0]->id;
                $canJoinSession = $this->global_join_session;
            } else {
                $currentUserId = null;
                $canJoinSession = null;
            }

            $this->eventrouter_model->currentUserId = $currentUserId;
            $this->eventrouter_model->canJoinSession = $canJoinSession;

            $this->eventrouter_model->slug = $this->page;
            $this->eventrouter_model->getEvent($context);
            $this->pageHtml = $this->eventrouter_model->pageHtml;
            
            if($this->eventrouter_model->event)
            {
                $this->eventData = $this->eventrouter_model->event[0];
                $this->global_bread_crumbs[$this->eventData->name] = $this->eventData->url_segment;

                if($this->addCrumb)
                {
                    $this->global_bread_crumbs[$this->addCrumb['title']] = $this->eventData->url_segment.'/'.$this->addCrumb['segment'];
                }

                $this->global_google_map_js = $this->eventrouter_model->mapJs;
                $this->tpl_data['event'] = $this->eventData;
            } else {
                $this->eventData = null;
            }
        }
    }


    /**
     * -------------------------------------------------------------------------
     * renders a specific event index page
     * -------------------------------------------------------------------------
     *
     * @access private
     * @return view
    **/
    private function eventIndex ()
    {
        if($this->page)
        {
            $this->setEvent('index');

            $this->global_active_nav = 'overview';
            $this->tpl_data['pageHtml'] = $this->pageHtml;

            $js[] = array('file'=>'filter-presenters.js');
            $this->tpl_data['add_js'] = add_js($js);
            $this->tpl_view = 'event';
            $this->render_tpl();
        }
    }


    /**
     * -------------------------------------------------------------------------
     * prepare the view for rendering
     * -------------------------------------------------------------------------
     *
     * @access private
     * @return view $this->render
    **/
    private function contact ()
    {
        if($this->page)
        {
            $context = 'contact';
            $this->setEvent($context);

            $this->global_active_nav = $context;
            $this->tpl_data['pageHtml'] = $this->pageHtml;
            // $this->tpl_data['event'] = $this->eventData;
            $eventContactEmail = $this->eventrouter_model->eventContactEmail;
            $this->tpl_data['postUrl'] = project_url().$this->page.'/contact';

            if (!empty($_POST))
            {
                if(isset($_POST['send']))
                {
                    $this->form_validation->set_rules('full_name', 'Full Name', 'trim|required|max_length[255]|xss_clean');
                    $this->form_validation->set_rules('company', 'Organization', 'trim|max_length[255]|xss_clean');
                    $this->form_validation->set_rules('title', 'Job Title', 'trim|max_length[255]|xss_clean');
                    $this->form_validation->set_rules('email', 'Email Address', 'trim|required|valid_email|max_length[255]|xss_clean');
                    $this->form_validation->set_rules('message', 'Message', 'trim|required|xss_clean');

                    // Process the form if validation is ok
                    if($this->form_validation->run() == TRUE)
                    {
                        $body  = '<p>Full Name: '.$this->input->post('full_name').'</p>';
                        
                        if(strlen($this->input->post("company")) > 0)
                        {
                            $body .= '<p>Company: '.$this->input->post("company").'</p>';
                        }
                        
                        if(strlen($this->input->post("title")) > 0)
                        {
                            $body .= '<p>Job Title: '.$this->input->post("title").'</p>';
                        }

                        $body .= '<p>Email: '.$this->input->post('email').'</p>';
                        $body .= '<p>Message: '.$this->input->post('message').'</p>';

                        $email_data = array(
                            'type'    => 'contact-page-message',
                            'from'    => 'ip@oetc.org',
                            'to'      => $eventContactEmail,
                            'subject' => $this->eventData->name.' Contact Page Message',
                            'body'    => $body
                        );

                        if($this->email_model->send_email($email_data, null, null, false))
                        {
                            $this->global_alert_success[] = 'Your message has been sent.';
                        } else {
                            $this->global_alert_error[] = 'Unable to send your message.  Please try again.';
                        }
                    } else {
                        $this->global_alert_error[] = validation_errors();
                    }
                }
            }

            $this->tpl_view = $context;
            $this->render_tpl();
        }
    }


    /**
     * -------------------------------------------------------------------------
     * prepare the view for rendering
     * -------------------------------------------------------------------------
     *
     * @access private
     * @param string $slug is the segment for a specific session like session/how-to-code
     * @return view $this->render
    **/
    private function session ()
    {
        $context = 'session';
        $this->setEvent($context);
        $this->global_active_nav = $context;

        if(isset($this->params[1]) AND strlen($this->params[1]) > 0)
        {
            $this->eventrouter_model->slug = $this->page;
            $this->eventrouter_model->sessionSlug = $this->params[1];
            $this->eventrouter_model->getSession();
            $this->tpl_data['pageHtml'] = $this->eventrouter_model->pageHtml;
        } else {
            redirect($this->page);
        }

        $this->tpl_view = $context;
        $this->render_tpl();
    }


    /**
     * -------------------------------------------------------------------------
     * prepare the session content for a jQuery load()
     * -------------------------------------------------------------------------
     *
     * @access private
     * @param string $slug is the segment for a specific session like session/how-to-code
     * @return view $this->render
    **/
    private function sessionContent ()
    {
        $context = 'session';
        $this->setEvent($context);

        if(isset($this->params[1]) AND strlen($this->params[1]) > 0)
        {
            $this->eventrouter_model->slug = $this->page;
            $this->eventrouter_model->sessionSlug = $this->params[1];

            // false returns the 'light' html version of the view
            $this->eventrouter_model->getSession(false);
            
            echo $this->eventrouter_model->pageHtml;
        }
    }


    /**
     * -------------------------------------------------------------------------
     * prepare the attendee101 view for rendering
     * -------------------------------------------------------------------------
     *
     * @access private
     * @return view $this->render
    **/
    private function attendee ()
    {
        if($this->page)
        {
            $context = 'attendee';
            $this->setEvent($context);

            if($this->pageHtml)
            {
                $this->global_active_nav = $context;
                $this->tpl_data['pageHtml'] = $this->pageHtml;
                $this->tpl_data['event'] = $this->eventData;

                $this->tpl_view = $context;
                $this->render_tpl();
            } else {
                $this->noroute();
            }
        }
    }


    /**
     * -------------------------------------------------------------------------
     * prepare the event map view for rendering
     * -------------------------------------------------------------------------
     *
     * @access private
     * @return view $this->render
    **/
    private function map ()
    {
        if($this->page)
        {
            $context = 'map';
            $this->setEvent($context);

            $this->global_active_nav = $context;
            $this->tpl_data['pageHtml'] = $this->pageHtml;
            $this->tpl_data['event'] = $this->eventData;

            $this->tpl_view = $context;
            $this->render_tpl();
        }
    }


    /**
     * -------------------------------------------------------------------------
     * loads view to record an attendee visit
     * -------------------------------------------------------------------------
     *
     * @access private
     * @return view $this->render
    **/
    private function connect ()
    {
        if($this->global_issue_tickets == true)
        {
            $sponsorId = null;

            if($this->global_user)
            {
                $sponsorId = $this->global_user[0]->sponsor_id;
            }

            if($this->page)
            {
                $context = 'connect';
                $this->eventrouter_model->sponsorId = $sponsorId;
                $this->setEvent($context);

                $this->global_active_nav = $context;
                $this->tpl_data['pageHtml'] = $this->pageHtml;
                $this->tpl_data['event'] = $this->eventData;

                $js[] = array('file' => 'connections.js');
                $this->tpl_data['add_js'] = add_js($js);
                $this->tpl_data['datatables'] = true;
                $this->tpl_view = $context;
                $this->render_tpl();
            }
        } else {
            $this->noroute();
        }
    }


    /**
     * -------------------------------------------------------------------------
     * prepare the view for rendering
     * -------------------------------------------------------------------------
     *
     * @access private
     * @param string $slug is the segment for a specific bio like bio/derek-nutile
     * @return view $this->render
    **/
    private function bio ()
    {
        $context = 'bio';

        if(isset($this->params[1]) AND strlen($this->params[1]) > 0)
        {
            $this->eventrouter_model->userSlug = $this->params[1];
        } else {
            redirect($this->page);
        }

        $this->global_active_nav = $context;
        $this->setEvent($context);
        $this->tpl_data['quicksearch'] = true;
        $js[] = array('file'=>'filter-events.js');
        $this->tpl_data['add_js'] = add_js($js);
        $this->tpl_data['pageHtml'] = $this->pageHtml;
        $this->tpl_data['programTable'] = $this->eventrouter_model->programTable;

        $this->tpl_view = $context;
        $this->render_tpl();
    }


    /**
     * -------------------------------------------------------------------------
     * prepare the view for rendering
     * -------------------------------------------------------------------------
     *
     * @access private
     * @return view $this->render
    **/
    private function presenters ($slug = null)
    {
        $this->global_active_nav = 'presenters';
        $this->addCrumb = array('title'=>'All Presenters','segment'=>'presenters');
        $this->setEvent('presenters');
        $this->tpl_data['pageHtml'] = $this->pageHtml;

        $js[] = array('file'=>'filter-presenters.js');
        $this->tpl_data['add_js'] = add_js($js);
        $this->tpl_data['quicksearch'] = true;

        $this->tpl_view = 'presenters';
        $this->render_tpl();
    }


    /**
     * -------------------------------------------------------------------------
     * prepare the view for rendering
     * -------------------------------------------------------------------------
     *
     * @access private
     * @return view $this->render
    **/
    private function schedule ()
    {
        $context = 'schedule';
        
        $this->setEvent($context);
        $this->tpl_data['eventUrl'] = project_url().$this->page;
        $this->tpl_data['context'] = $context;
        $this->tpl_data['pageHtml'] = $this->eventrouter_model->programTable;

        $this->global_active_nav = $context;
        $js[] = array('file'=>'filter-events.js');
        $this->tpl_data['add_js'] = add_js($js);
        $this->tpl_data['quicksearch'] = true;

        $this->tpl_view = $context;
        $this->render_tpl();
    }


    /**
     * -------------------------------------------------------------------------
     * prepare the view for rendering
     * -------------------------------------------------------------------------
     *
     * @access private
     * @return view $this->render
    **/
    private function mine ()
    {
        $context = 'mine';
        
        $this->setEvent($context);
        $this->tpl_data['eventUrl'] = project_url().$this->page;
        $this->tpl_data['context'] = $context;
        $this->tpl_data['pageHtml'] = $this->eventrouter_model->programTable;

        $this->global_active_nav = 'mine';
        $js[] = array('file'=>'filter-events.js');
        $this->tpl_data['add_js'] = add_js($js);
        $this->tpl_data['quicksearch'] = true;

        $this->tpl_view = 'schedule';
        $this->render_tpl();
    }


    /**
     * -------------------------------------------------------------------------
     * prepare the view for rendering
     * -------------------------------------------------------------------------
     *
     * @access private
     * @return view $this->render
    **/
    private function sponsors ()
    {
        $context = 'sponsors';

        $this->global_active_nav = $context;
        $this->setEvent($context);
        $this->tpl_data['pageHtml'] = $this->pageHtml;

        $this->tpl_view = $context;
        $this->render_tpl();
    }


    /**
     * -------------------------------------------------------------------------
     * prepare the view for rendering
     * -------------------------------------------------------------------------
     *
     * @access private
     * @return view $this->render
    **/
    private function prizes ()
    {
        $context = 'prizes';

        $this->global_active_nav = $context;
        $this->setEvent($context);
        $this->tpl_data['pageHtml'] = $this->pageHtml;

        $this->tpl_view = $context;
        $this->render_tpl();
    }


    /**
     * -------------------------------------------------------------------------
     * prepare the view for rendering
     * -------------------------------------------------------------------------
     *
     * @access private
     * @return view $this->render
    **/
    private function travel ()
    {
        if($this->page)
        {
            $context = 'travel';
            $this->setEvent($context);

            if($this->pageHtml)
            {
                $this->global_active_nav = $context;
                $this->tpl_data['pageHtml'] = $this->pageHtml;
                $this->tpl_data['event'] = $this->eventData;

                $this->tpl_view = $context;
                $this->render_tpl();
            } else {
                $this->noroute();
            }
        }
    }


    /**
     * -------------------------------------------------------------------------
     * prepare the view for rendering
     * -------------------------------------------------------------------------
     *
     * @access public
     * @return view $this->render
    **/
    public function json ()
    {
        $this->view = $this->router->method;
        $this->load->view($this->view);
    }


    /**
     * -------------------------------------------------------------------------
     * no route found -- 404 error message
     * -------------------------------------------------------------------------
     *
     * @access private
     * @return view $this->render
    **/
	private function noroute ($page = null)
	{
        if($this->page)
        {
            $message = "The page '".strtoupper($this->page)."' does not exist.";
        } else {
            $message = "It looks like the page you're looking for is no longer here.";
        }

        $this->global_page_header = page_header("Page Not Found", $message);

        $this->tpl_view = 'noroute';
        $this->tpl_navbar_header = 'navbar_header';
        $this->render_tpl();
	}


    /**
     * -------------------------------------------------------------------------
     * if the route starts with this class name (ie. "page")then reroute without
     * the class prefix ...
     * -------------------------------------------------------------------------
     *
     * @access private
     * @return void
    **/
	private function reroute ()
	{
        if($this->page == $this->router->class)
        {
    		if($this->uri->total_segments() > 1)
    		{
    			$this->load->helper('url');
    			/** 
    			 * parse the uri string and remove the "page/" portion
    			**/
    			$uri = substr($this->uri->uri_string, strlen($this->page)+1);
    			redirect($uri);
    		} else {
    			$this->noroute($this->page);
    		}
        }
	}


    /**
     * -------------------------------------------------------------------------
     * render a google map
     * -------------------------------------------------------------------------
     *
     * @access private
     * @param array $settings
     * @param array $marker
     * @return view
    **/
    private function renderMap ($settings = null, $marker = null)
    {   
        if($settings)
        {
            $this->load->library('googlemaps');
            $this->googlemaps->initialize($settings);

            if($marker)
            {
                $this->googlemaps->add_marker($marker);
            }
            
            $map = $this->googlemaps->create_map();

            $this->global_google_map_js = $map['js'];
            $this->global_google_map_html = $map['html'];
        }
    }


    /**
     * -------------------------------------------------------------------------
     * toggle the connection
     * -------------------------------------------------------------------------
     *
     * @access public
     * @return void
    **/
    public function setConnection ()
    {
        $message = null;
        $userId = $this->input->post('userId');
        $eventId = $this->input->post('eventId');
        $sponsorId = $this->input->post('sponsorId');
        $setVisit = $this->input->post('setVisit');

        if($eventId AND $sponsorId)
        {
            if($setVisit == 1)
            {
                $set = true;
            } else {
                $set = false;
            }

            if($this->eventrouter_model->setUserVisit($userId, $eventId, $sponsorId, $set))
            {
                $message = 'Flagged userId:'.$eventId.' to visited for sponsorId: '.$sponsorId;
            } else {
                $message = 'Flagged userId:'.$eventId.' to UNvisited for sponsorId: '.$sponsorId;
            }
        } else {
            $message = 'Invalid';
        }

        $array = array("result" => $message);
        echo json_encode($array);
    }


    /**
     * -------------------------------------------------------------------------
     * connection summary
     * -------------------------------------------------------------------------
     *
     * @access public
     * @return void
    **/
    public function connectionSummary ()
    {
        $this->eventrouter_model->slug = $this->page;   
        $this->eventrouter_model->setEventId();
        $this->eventrouter_model->sponsorId = $this->params[1];
        $this->eventrouter_model->buildSponsorConnectSummary();

        $summary = $this->eventrouter_model->sponsorConnectSummary;

        if($summary)
        {
            echo $summary;
        } else {
            echo body_message('No summary.');
        }
    }


    /**
     * -------------------------------------------------------------------------
     * update a session over ajax
     * -------------------------------------------------------------------------
     *
     * @access public
     * @return void
    **/
    public function updateSessionAjax ()
    {
        $message = null;
        $userId = $this->input->post('userId');
        $sessionId = $this->input->post('sessionId');
        $setAttendance = $this->input->post('setAttendance');

        if($userId AND $sessionId)
        {
            if($setAttendance == 1)
            {
                $set = true;
            } else {
                $set = false;
            }

            if($this->eventrouter_model->setUserSessionAttendance($userId, $sessionId, $set))
            {
                $message = 'Success setting user:'.$userId.' and session: '.$sessionId.' to: '.$set;
            } else {
                $message = 'Failed setting user:'.$userId.' and session: '.$sessionId.' to: '.$set;
            }
        } else {
            $message = 'Invalid';
        }

        $array = array("result" => $message);
        echo json_encode($array);
    }
}

/* End of file eventrouter.php */
/* Location: ./application/modules/eventrouter/controllers/eventrouter.php */
