<?php
/**
 * standalone script to grab leads and send to ExactTarget in CSV format
**/

class Lead {

    /**
     * -------------------------------------------------------------------------
     * class variables
     * -------------------------------------------------------------------------
    **/
    private $leadsDirectoryPath = "/path/to/dump/";
	private $files = array(); // collection of file names
	private $results = array(); // parsed sql results go here


    /**
     * -------------------------------------------------------------------------
     * database variables
     * -------------------------------------------------------------------------
    **/

	// production
	private $hostname = "hostname";
	private $username = "username";
	private $password = "password";
	private $database = "database";

	// development
    // private $hostname = "hostname";
    // private $username = "username";
    // private $password = "password";
    // private $database = "database";


    /**
     * -------------------------------------------------------------------------
     * ftp variables
     * -------------------------------------------------------------------------
    **/
	private $ftpHost = 'ipaddress';
	private $ftpUser = 'ftpuser';
	private $ftpPass = 'ftppass';


    /**
     * -------------------------------------------------------------------------
     * SQL to File column translation map
     * -------------------------------------------------------------------------
    **/
	private $columnMap = array(
		// SQL Field Name 			=> File field name
		'user_id' 					=> 'user_id',
		'lead_date' 				=> 'lead_date', // Reformat %m%d%Y %H:%i:%s
		'fname'						=> 'fname',
		'lname'						=> 'lname',
		'email_address' 			=> 'email_address',
		'address'					=> 'address',
		'city'						=> 'City',
		'state'						=> 'State',
		'zip' 						=> 'Zip',
		'country' 					=> 'Country',
		'home_phone'				=> 'home_phone', // Is this correct???
		'mobile_phone'				=> 'mobile_phone',
		'work_phone'				=> 'work_phone',
		'fax_no'					=> 'fax_no',
		'company' 					=> 'Company',
		'preferred_contact_time' 	=> 'preferred_contact_time',                        
		'receive_mc_email'			=> 'receive_mc_email',
		'receive_partner_email'		=> 'receive_partner_email',
		'lead_referrer'				=> 'lead_referrer', 
		'lead_timezone'				=> 'lead_timezone',
		'dealerno' 					=> 'Dealerno',
		'clientele_key' 			=> 'clientele_key',
		'dab_id'					=> 'dab_id',
		'boat_model' 				=> 'boat_model',
		'interested_series1' 		=> 'interested_series1',
		'interested_model1' 		=> 'interested_model1',
		'interested_series2' 		=> 'interested_series2',
		'interested_model2'			=> 'interested_model2',
		'purchase_time' 			=> 'purchase_time',
		'boat_use' 					=> 'boat_use',
		'trade_in' 					=> 'trade_in',
		'family_income' 			=> 'family_income',
		'boat_owner' 				=> 'boat_owner',
		'boat_brand' 				=> 'boat_brand',
		'boat_manufacturer' 		=> 'boat_manufacturer',
		'comments'					=> 'comments' 
	);


    /**
     * -------------------------------------------------------------------------
     * default constructor
     * -------------------------------------------------------------------------
    **/
	public function __construct ()
	{
		date_default_timezone_set('America/Los_Angeles');

		/**
		 * Note: there's really no logic here to handle the what-if-nots
		**/
		if($this->setDatabaseConnection()) // did we connect to the db?
		{	
			$this->queryForNewLeads();
			$this->createLeadFiles();

			if(count($this->files) > 0) // did we create lead files?
			{
				$this->sendLeadFiles();
			}
		}
	}


    /**
     * -------------------------------------------------------------------------
     * connect to the database
     * -------------------------------------------------------------------------
     *
     * @access private
     * @return boolean
    **/
	private function setDatabaseConnection ()
	{
		$return = false;

		$link = mysql_connect($this->hostname, $this->username, $this->password);
		
		if($link)
		{
			mysql_select_db($this->database);
			$return = true;
		} else {
			$this->log("Unable to connect to the database: ".$this->hostname, 'errors.txt');
		}

		return $return;
	}


    /**
     * -------------------------------------------------------------------------
     * query for new leads and set $this->results
     * -------------------------------------------------------------------------
     *
     * @access private
     * @return boolean
    **/
    private function queryForNewLeads ()
    {
		$sql = "SELECT * FROM leads WHERE lead_exported = 'N' AND lead_referrer != 'info_request'";
		$rows = mysql_query($sql);

		while($row = mysql_fetch_assoc($rows))
		{
			$this->results[] = $row;
		}
	}


    /**
     * -------------------------------------------------------------------------
     * turn the $this->results array into csv file(s)
     * -------------------------------------------------------------------------
     *
     * @access private
     * @param boolean $makeHeaderRow if true includes a header row in the CSV
     * @param string $fileName is generated automatically if left blank - ''
     * @return boolean
    **/
    private function createLeadFiles ($makeHeaderRow = true, $fileName = '')
    {
    	$return = false;

    	if($this->results)
    	{
	        foreach($this->results AS $result)
	        {
	        	$array = array();
            	$headerRow = array();
            	$dataRow = array();

            	/**
            	 * set the header row (optional)
            	**/
		        if($makeHeaderRow)
		        {
		            foreach($this->columnMap AS $sqlColumn => $fileColumn)
		            {
		                $headerRow[] = $fileColumn;
		            }
		            $array[] = $headerRow;
		        }

            	/**
            	 * add the data row
            	**/
				foreach($this->columnMap AS $sqlColumn => $fileColumn)
				{
					if($sqlColumn == 'country')
					{
						if($result[$sqlColumn] == 'US' OR $result[$sqlColumn] == 'USA')
						{
							$dataRow[] = 'United States';
						}else{
							$dataRow[] = $result[$sqlColumn];
						}
					} elseif($sqlColumn == 'lead_date' ) {
						// 2013-06-25 19:47:17 => 06242013 02:15:46
						$dataRow[] = substr($result[$sqlColumn],5,2).substr($result[$sqlColumn],8,2).substr($result[$sqlColumn],0,4).' '.substr($result[$sqlColumn],11,8);
					} else {
						$dataRow[] = $result[$sqlColumn];
					}
				}
	            $array[] = $dataRow;

            	/**
            	 * write the csv file and set the lead to exported
            	**/
		        if($array)
		        {
			        $counter = 0;
		        	$fileName = 'lead_'.$result['lead_id'].'.csv';
		        	$fullPathAndFile = $this->leadsDirectoryPath.$fileName;
					$fp = fopen($fullPathAndFile, 'w+');

			        foreach($array as $rows)
			        {
			            $counter++;
	    				fputcsv($fp, $rows);
			        }

					fclose($fp);
					$return = true;
					$this->files[] = $fileName;
					$this->setLeadToExported($result['lead_id']);
		        }

	            $array[] = $lines;
	        }
	    }

	    return $return;
    }


    /**
     * -------------------------------------------------------------------------
     * send lead files
     * -------------------------------------------------------------------------
     *
     * @access private
     * @return boolean
    **/
	private function sendLeadFiles ()
	{
		if($this->files)
		{
			error_reporting( E_ALL );

	        foreach($this->files AS $file)
	        {
	        	$fileAndPath = $this->leadsDirectoryPath.$file;

				if(is_file($fileAndPath))
				{
					echo "Is File";
					die();

					$ftpConnection = ftp_connect($this->ftpHost);
					$ftpLogin = ftp_login($ftpConnection, $this->ftpUser, $this->ftpPass);
					ftp_pasv($ftpConnection,true);

					if(($ftpLogin))
					{
						if(ftp_put($ftpConnection, $file, $fileAndPath, FTP_BINARY))
						{
							$this->log("Lead file ".$file." was uploaded to ".$this->ftpHost, 'log.txt');
						}
					} else {
						$this->log("Unable to connect to the ftp server: ".$this->ftpHost, 'errors.txt');
					}

					ftp_close($ftpConnection);
				}
	        }
		}
	}


    /**
     * -------------------------------------------------------------------------
     * sets the lead to lead_exported = 'Y' in the database
     * -------------------------------------------------------------------------
     *
     * @access private
     * @param integer $id of the lead in the leads table
     * @return boolean
    **/
	private function setLeadToExported ($id = null)
	{
		if($id)
		{
			$update_query = "UPDATE leads SET lead_exported = 'Y' WHERE lead_id = ".$id;
			mysql_query($update_query);
			$this->log("Lead #".$id." was exported on ".date('Y-m-d H:i:s', 'log.txt'));
		}
	}


    /**
     * -------------------------------------------------------------------------
     * simple logger
     * -------------------------------------------------------------------------
     *
     * @access private
     * @param string $message
     * @param string $fileName is the name of the destination log file
     * @param string $mode is the fopen mode like a, a+, w, w+, etc.
     * @return boolean
    **/
	private function log ($message, $fileName = 'log.txt', $mode = 'a+')
	{
		$logFile = fopen($this->leadsDirectoryPath.$fileName, $mode);

		fwrite($logFile, $message);
		fwrite($logFile,"\n---------------------------------------------------------------------------\n");
		fclose($logFile);
	}
}

// just do it
$leads_class = new Lead();


/* End of file */
/* Location: ./application/solo */
