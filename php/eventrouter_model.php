<?php

class Eventrouter_model extends CI_Model
{
    /**
     * -------------------------------------------------------------------------
     * module variables
     * -------------------------------------------------------------------------
    **/
    private $module_class    = '';   // sets the class
    private $module_config   = null; // reads the module config/config.php file
    private $module_name     = '';   // module name
    private $module_navroot  = '';   // sets nav classes to active
    private $module_viewpath = '';   // path to view files


    /**
     * -------------------------------------------------------------------------
     * table variables
     * -------------------------------------------------------------------------
    **/
    private $table_banners                  = 'banners';
    private $table_cms_content              = 'cms_content';
    private $table_cms_content_shortcodes   = 'cms_content_shortcodes';
    private $table_events                   = 'events';
    private $table_events_banners           = 'events_banners';
    private $table_events_pallets           = 'events_pallets';
    private $table_events_locations         = 'events_locations';
    private $table_events_prizes            = 'events_prizes';
    private $table_events_sessions          = 'events_sessions';
    private $table_events_sponsors          = 'events_sponsors';
    private $table_events_strands           = 'events_strands';
    private $table_events_visits            = 'events_visits';
    private $table_locations                = 'locations';
    private $table_locations_coords         = 'locations_coords';
    private $table_locations_rooms          = 'locations_rooms';
    private $table_migrations               = 'migrations';
    private $table_pallets                  = 'pallets';
    private $table_permissions              = 'permissions';
    private $table_roles                    = 'roles';
    private $table_roles_permissions        = 'roles_permissions';
    private $table_rooms                    = 'rooms';
    private $table_sessions                 = 'sessions';
    private $table_sessions_presenters      = 'sessions_presenters';
    private $table_sessions_resources       = 'sessions_resources';
    private $table_sessions_strands         = 'sessions_strands';
    private $table_sponsors                 = 'sponsors';
    private $table_site_settings            = 'config_site';
    private $table_states                   = 'states';
    private $table_statuses                 = 'statuses';
    private $table_strands                  = 'strands';
    private $table_users                    = 'users';
    private $table_users_events             = 'users_events';
    private $table_users_roles              = 'users_roles';
    private $table_users_import             = 'users_import';
    private $table_users_sessions           = 'users_sessions';


    /**
     * -------------------------------------------------------------------------
     * public variables
     * -------------------------------------------------------------------------
    **/
    public $currentUserId                   = null;
    public $canJoinSession                  = false;
    public $event                           = null;  // event object
    public $eventContactEmail               = '';
    public $filter                          = null;
    public $mapJs                           = null;
    public $mapHtml                         = null;
    public $pageHtml                        = null; // html output for a page
    public $programTable                    = null;
    public $slug                            = null; // url segment like 'ipdx13' in site.com/ipdx13
    public $sessionSlug                     = null;
    public $sponsorConnectSummary           = null;
    public $strands                         = null;
    public $userSlug                        = null;


    /**
     * -------------------------------------------------------------------------
     * private variables
     * -------------------------------------------------------------------------
    **/
    private $checkedInCount                 = 0;
    private $cmsContent                     = null;
    private $eventId                        = null;
    private $eventPresenters                = null;
    private $eventSession                   = null;
    private $eventSessions                  = null;
    private $eventSponsors                  = null;
    private $mapAddress                     = null;
    private $postedData                     = null;
    private $prizes                         = null;
    private $sponsor                        = null;
    private $sponsorConnectTable            = null;
    private $timeSteps                      = 15;
    private $users                          = null;
    private $eventVisits                    = 0;
    private $userArray                      = null;


    /**
     * -------------------------------------------------------------------------
     * default constructor
     * -------------------------------------------------------------------------
    **/
	public function __construct()
	{
        parent::__construct();
    }


    /**
     * -------------------------------------------------------------------------
     * unset and set posted data
     * -------------------------------------------------------------------------
     *
     * @access public
     * @param array $data
     * @return $this->postedData
    **/
    public function setPostedData ($data = null)
    {
        $this->postedData = null;
        $this->postedData = $data;
    }


    /**
     * -------------------------------------------------------------------------
     * check the existence of an event url slug
     * -------------------------------------------------------------------------
     *
     * @access public
     * @param $enabled_only will return only an enabled url
     * @return boolean
    **/
    public function testEventUrlSegment ($enabled_only = true)
    {
        $return = false;

        if($this->slug)
        {
            /**
                SELECT evt.*, sts.name
                FROM events AS evt
                LEFT JOIN statuses AS sts ON sts.id = evt.status_id
                WHERE evt.url_segment = 'isf'
             */
            $query = $this->db->from($this->table_events.' AS evt');

            if($enabled_only == true)
            {
                $query = $this->db->join($this->table_statuses.' AS sts', 'sts.id = evt.status_id', 'left');
            }

            $query = $this->db->where('evt.url_segment', $this->slug);
            $query = $this->db->get();

            if ($query->num_rows() > 0)
            {
                $return = true;
            }
        }

        return $return;
    }


    /**
     * -------------------------------------------------------------------------
     * get an event by id
     * -------------------------------------------------------------------------
     *
     * @access public
     * @param string $context like presenters
     * @return object $this->event
    **/
    public function getEvent ($context = 'index')
    {
        /**
            SELECT evt.*, sts.name AS status, loc.id  AS location_id, loc.name AS location, loc.address, loc.city, loc.zipcode, ste.name_long AS state, 
            loc.website,  lcd.center, lcd.zoom, lcd.height, lcd.width, lcd.marker_position, lcd.marker_icon,
            bns.id AS banner_id, bns.class AS banner_class, pts.id AS pallet_id, pts.class AS pallet_class
            FROM events AS evt
            LEFT JOIN statuses AS sts ON sts.id = evt.status_id
            LEFT JOIN events_locations AS evl ON evl.event_id = evt.id
            LEFT JOIN locations AS loc ON loc.id = evl.location_id
            LEFT JOIN locations_coords AS lcd ON lcd.location_id = loc.id
            LEFT JOIN events_banners AS evb ON evb.event_id = evt.id
            LEFT JOIN banners AS bns ON bns.id = evb.banner_id
            LEFT JOIN events_pallets AS evp ON evp.event_id = evt.id
            LEFT JOIN pallets AS pts ON pts.id = evp.pallet_id
            LEFT JOIN states AS ste ON ste.id = loc.state_id
            WHERE evt.id = 2
         */
        $query = $this->db->select('evt.*, sts.name AS status, loc.id  AS location_id, loc.name AS location, loc.address, loc.city, loc.zipcode, ste.name_long AS state, 
            loc.website,  lcd.center, lcd.zoom, lcd.height, lcd.width, lcd.marker_position, lcd.marker_icon,
            bns.id AS banner_id, bns.class AS banner_class, pts.id AS pallet_id, pts.class AS pallet_class');
        $query = $this->db->from($this->table_events.' AS evt');
        $query = $this->db->join($this->table_statuses.' AS sts', 'sts.id = evt.status_id', 'left');
        $query = $this->db->join($this->table_events_locations.' AS evl', 'evl.event_id = evt.id', 'left');
        $query = $this->db->join($this->table_locations.' AS loc', 'loc.id = evl.location_id', 'left');
        $query = $this->db->join($this->table_locations_coords.' AS lcd', 'lcd.location_id = loc.id', 'left');
        $query = $this->db->join($this->table_events_banners.' AS evb', 'evb.event_id = evt.id', 'left');
        $query = $this->db->join($this->table_banners.' AS bns', 'bns.id = evb.banner_id', 'left');
        $query = $this->db->join($this->table_events_pallets.' AS evp', 'evp.event_id = evt.id', 'left');
        $query = $this->db->join($this->table_pallets.' AS pts', 'pts.id = evp.pallet_id', 'left');
        $query = $this->db->join($this->table_states.' AS ste', 'ste.id = loc.state_id', 'left');
        $query = $this->db->where('evt.url_segment', $this->slug);
        $query = $this->db->get();

        if ($query->num_rows() > 0)
        {
            $this->event = $query->result();
            $this->setEventId();

            switch ($context)
            {
                case 'attendee':
                    $this->buildCmsPageHtml('attendee_101');
                    break;

                case 'bio':
                    $this->makeBioHtml();
                    $this->buildSchedule();
                    break;

                case 'connect':
                    $this->makeConnectPageHtml();
                    break;

                case 'contact':
                    $this->getContactDetails();
                    $this->setContactEmailAddress();
                    break;

                case 'index':
                    $this->getStrands();
                    $this->buildCmsPageHtml();
                    break;

                case 'map':
                    $this->pageHtml = '';
                    break;

                case 'mine':
                    $this->buildSchedule(true);
                    break;

                case 'presenters':
                    $this->pageHtml = $this->shortcodePresenters();
                    break;

                case 'prizes':
                    $this->makePrizePageHtml();
                    break;

                case 'schedule':
                    $this->buildSchedule();
                    break;

                case 'sponsors':
                    $this->pageHtml = $this->shortcodeSponsors();
                    break;

                case 'travel':
                    $this->buildCmsPageHtml('travel_html');
                    break;
                
                default:
                    $this->getStrands();
                    $this->buildCmsPageHtml();
                    break;
            }
        }
    }


    /**
     * -------------------------------------------------------------------------
     * gather cms block rows for a certain context like events and event id
     * -------------------------------------------------------------------------
     *
     * @access private
     * @param boolean $personal will show filter the schedule for a user if true
     * @return string $this->programTable
     */
    private function buildSchedule ($personal = false)
    {
        if($this->event AND $this->slug)
        {
            $this->getSessionsByEvent();
            
            if($this->eventSessions)
            {
                $html = '';
                $sessionRow = '';
                $event_status = $this->event[0]->status;
                $days = calculate_days($this->event[0]->start_date, $this->event[0]->end_date)+1;

                for($i=0; $i<$days; ++$i)
                {
                    $date_to_array = date_to_array($this->event[0]->start_date);
                    $date = mktime(0, 0, 0, date($date_to_array['month']), date($date_to_array['day'])+$i, date($date_to_array['year']));
                    $thisDate = pretty_date($date, 'Y-m-d');
                    $times = time_array($this->timeSteps);

                    $html .= '<div class="event-date-header">'.pretty_date($date).'</div>';
                    $dateHasSessions = false; // assume this day has no sessions

                    foreach($times AS $key => $val)
                    {
                        foreach($this->eventSessions AS $session)
                        {
                            $session_date = pretty_date($session->start_date, 'Y-m-d');

                            if($session_date == $thisDate)
                            {
                                $start_time = pretty_date($session->start_date, 'H:i:00');
                                $end_time = pretty_date($session->end_date, 'H:i:00');

                                if($key == $start_time)
                                {
                                    $sessionDetails = '';
                                    $presenters = null;
                                    $names = array();
                                    $session_presenters = $this->getPresentersBySessionId($session->id);

                                    if(strlen($session->room_name) > 0)
                                    {
                                        $room = tbui_icon('map-marker').' '.anchor(project_url().$this->slug."/map/", $session->room_name, 'target="_blank"').' ';
                                    } else {
                                        $room = '';
                                    }

                                    if($session_presenters)
                                    {
                                        $presenters = tbui_icon('user');

                                        foreach($session_presenters AS $presenter)
                                        {
                                            $presenters .= anchor(project_url().$this->slug."/bio/".$presenter->url, $presenter->first_name.' '.$presenter->last_name, 'target="_blank"').' ';
                                        }
                                    } else {
                                        $presenters = '&nbsp;';
                                    }

                                    $sessionDetails = trim($room.' '.$presenters);

                                    $start = pretty_date($session->start_date, 'g:i A');
                                    $end = pretty_date($session->end_date, 'g:i A');
                                    $sessionLink = anchor(project_url().$this->slug."/session/".$session->id, $session->name, 'class="showSession" target="_blank" data-loadUrl="'.project_url().$this->slug."/sessionContent/".$session->id.'" data-target="'.$session->id.'"');
                                    $addHtml = true;

                                    // test user_id and authentication before producing html
                                    // first, test to see if the user has the right to join
                                    if($this->canJoinSession == true AND $this->currentUserId)
                                    {
                                        // here, check current user status for this session
                                        $isAttending = $this->isUserAttendingSession($this->currentUserId, $session->id);
                                        $checkedState = '';

                                        if($isAttending == true)
                                        {
                                            $checkedState = 'checked="checked"';
                                        } else {
                                            if($personal == true)
                                            {
                                                $addHtml = false; // skip this because we are only showing the personal schedule
                                            } 
                                        }

                                        $checkBox = '
                                            <div class="make-switch switch-small event-toggler" data-eventUrl="'.$this->slug.'" data-sessionId="'.$session->id.'" data-userId="'.$this->currentUserId.'" data-on-label="<i class=\'icon-ok-sign icon-white\'></i>" data-off-label="<i class=\'icon-minus-sign\'></i>">
                                                <input type="checkbox" class="attend-session" id="input-session-'.$session->id.'"'.$checkedState.'>
                                            </div>';
                                    } else {
                                        if($personal == true)
                                        {
                                            $addHtml = false; // skip this because we are only showing the personal schedule
                                        } else {
                                            $checkBox = '
                                                <div class="make-switch switch-small event-toggler" data-on-label="<i class=\'icon-ok-sign icon-white\'></i>" data-off-label="<i class=\'icon-minus-sign\'></i>">
                                                    <input type="checkbox" disabled="disabled">
                                                </div>';
                                        }
                                    }

                                    if($addHtml == true)
                                    {
                                        $dateHasSessions = true;

                                        $html .= '
                                            <div class="row-fluid event-row">

                                                <div class="span3">
                                                    <div class="row-fluid">
                                                        <div class="span6 event-check">'.$checkBox.'</div>
                                                        <div class="span6 event-times">
                                                            <div class="event-start-time">'.$start.'</div>
                                                            <div class="event-end-time">'.$end.'</div>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="span9">
                                                    <div class="event-details">
                                                        <p class="event-title">'.$sessionLink.'</p>
                                                        <p class="event-presenters" id="presenters-'.$session->id.'">'.$sessionDetails.'</p>
                                                        <div class="sessionDisplays" id="session-'.$session->id.'" data-toggle="hide"></div>
                                                    </div>
                                                </div>

                                            </div>
                                        ';
                                    }
                                }
                            }
                        }
                    }
                    $this->programTable = $html;

                } // END each day loop
            }
        }   
    }


    /**
     * -------------------------------------------------------------------------
     * check an enabled session to see if a user is attending
     * -------------------------------------------------------------------------
     *
     * @access private
     * @param integer $userId
     * @param integer $sessionId
     * @return boolean
     */
    private function isUserAttendingSession ($userId = null, $sessionId = null)
    {
        $return = false;

        if($userId AND $sessionId)
        {
            /**
                SELECT uss.*
                FROM users_sessions AS uss
                JOIN sessions AS ses ON ses.id = uss.session_id
                LEFT JOIN statuses AS sts ON sts.id = ses.status_id
                WHERE uss.session_id = 141
                AND uss.user_id = 2
                AND sts.name = 'enabled'
            **/
            $query = $this->db->from($this->table_users_sessions.' AS uss');
            $query = $this->db->join($this->table_sessions.' AS ses', 'ses.id = uss.session_id');
            $query = $this->db->join($this->table_statuses.' AS sts', 'sts.id = ses.status_id', 'left');
            $query = $this->db->where('uss.session_id', $sessionId);
            $query = $this->db->where('uss.user_id', $userId);
            $query = $this->db->where('sts.name', 'enabled');
            $query = $this->db->get();

            if ($query->num_rows() > 0)
            {
                $return = true;
            }
        }

        return $return;
    }


    /**
     * -------------------------------------------------------------------------
     * toggle (set/unset) a user visit to a vendor/sponsor
     * -------------------------------------------------------------------------
     *
     * @access public
     * @param integer $userId
     * @param integer $sessionId
     * @param boolean $set will set or unset the user attendance
     * @return boolean
     */
    public function setUserVisit ($userId = null, $eventId = null, $sponsorId = null, $set = 1)
    {
        $return = false;

        if($userId AND $sponsorId AND $eventId)
        {
            if($set == 1)
            {
                $this->addUserVisit($userId, $sponsorId, $eventId);
            } else {
                $this->removeUserVisit($userId, $sponsorId, $eventId);
            }

            $return = true;
        }

        return $return;
    }


    /**
     * -------------------------------------------------------------------------
     * adds new or updates existing a user/session to the users_sessions table
     * -------------------------------------------------------------------------
     *
     * @access private
     * @param integer $userId
     * @param integer $sessionId
     * @return boolean
     */
    private function addUserVisit ($userId = null, $sponsorId = null, $eventId = null)
    {
        $return = false;

        if($userId AND $sponsorId AND $eventId)
        {
            $this->removeUserVisit($userId, $sponsorId, $eventId);
            
            $data = array(
                'user_id' => $userId,
                'sponsor_id' => $sponsorId,
                'event_id' => $eventId,
                'created' => get_now()
            );

            $insert = $this->db->insert($this->table_events_visits, $data);
        }

        return $return;
    }


    /**
     * -------------------------------------------------------------------------
     * delete a user/session match from the users_sessions table
     * -------------------------------------------------------------------------
     *
     * @access private
     * @param integer $userId
     * @param integer $sessionId
     * @return void
     */
    private function removeUserVisit ($userId = null, $sponsorId = null, $eventId = null)
    {
        if($userId AND $sponsorId AND $eventId)
        {
            $this->db->where('user_id', $userId);
            $this->db->where('sponsor_id', $sponsorId);
            $this->db->where('event_id', $eventId);
            $this->db->delete($this->table_events_visits);
        }
    }


    /**
     * -------------------------------------------------------------------------
     * toggle (set/unset) user attendance
     * -------------------------------------------------------------------------
     *
     * @access public
     * @param integer $userId
     * @param integer $sessionId
     * @param boolean $set will set or unset the user attendance
     * @return boolean
     */
    public function setUserSessionAttendance ($userId = null, $sessionId = null, $set = 1)
    {
        $return = false;

        if($userId AND $sessionId)
        {
            if($set == 1)
            {
                $this->addUserToSession($userId, $sessionId);
            } else {
                $this->removeUserFromSession($userId, $sessionId);
            }

            $return = true;
        }

        return $return;
    }


    /**
     * -------------------------------------------------------------------------
     * adds new or updates existing a user/session to the users_sessions table
     * -------------------------------------------------------------------------
     *
     * @access private
     * @param integer $userId
     * @param integer $sessionId
     * @return boolean
     */
    private function addUserToSession ($userId = null, $sessionId = null)
    {
        $return = false;

        if($userId AND $sessionId)
        {
            $isAttending = $this->isUserAttendingSession($userId, $sessionId);

            if($isAttending)
            {
                $updatedData = array(
                    'user_id' => $userId,
                    'session_id' => $sessionId,
                    'updated' => get_now()
                );

                $update = $this->db->where('user_id', $userId);
                $update = $this->db->where('session_id', $sessionId);
                $update = $this->db->update($this->table_users_sessions, $updatedData);
            } else {
                $newData = array(
                    'user_id' => $userId,
                    'session_id' => $sessionId,
                    'created' => get_now(),
                    'updated' => get_now()
                );

                $insert = $this->db->insert($this->table_users_sessions, $newData);
            }
        }

        return $return;
    }


    /**
     * -------------------------------------------------------------------------
     * delete a user/session match from the users_sessions table
     * -------------------------------------------------------------------------
     *
     * @access private
     * @param integer $userId
     * @param integer $sessionId
     * @return void
     */
    private function removeUserFromSession ($userId = null, $sessionId = null)
    {
        if($userId AND $sessionId)
        {
            $this->db->where('user_id', $userId);
            $this->db->where('session_id', $sessionId);
            $this->db->delete($this->table_users_sessions);
        }
    }


    /**
     * -------------------------------------------------------------------------
     * get sessions for an event
     * -------------------------------------------------------------------------
     *
     * @access private
     * @param integer $id
     * @return null or array
     */
    private function getPresentersBySessionId ($id = null)
    {
        $return = null;

        if($id)
        {
            /**
                SELECT usr.id, usr.first AS first_name, usr.last AS last_name, usr.url, usr.company, usr.title, usr.image, sts.name
                FROM events_sessions AS ess
                JOIN sessions AS ses ON ses.id = ess.session_id
                JOIN sessions_presenters AS sps ON sps.session_id = ess.session_id
                LEFT JOIN users AS usr ON usr.id = sps.user_id
                JOIN statuses AS sts ON sts.id = usr.status_id
                WHERE ses.id = 141
            **/
            $query = $this->db->select('usr.id, usr.first AS first_name, usr.last AS last_name, usr.url, usr.company, usr.title, usr.image, sts.name');
            $query = $this->db->from($this->table_events_sessions.' AS ess');
            $query = $this->db->join($this->table_sessions.' AS ses', 'ses.id = ess.session_id');
            $query = $this->db->join($this->table_sessions_presenters.' AS sps', 'sps.session_id = ess.session_id');
            $query = $this->db->join($this->table_users.' AS usr', 'usr.id = sps.user_id', 'left');
            $query = $this->db->join($this->table_statuses.' AS sts', 'sts.id = usr.status_id');
            $query = $this->db->where('ses.id', $id);
            $query = $this->db->get();

            if ($query->num_rows() > 0)
            {
                $return = $query->result();
            }
        }

        return $return;
    }


    /**
     * -------------------------------------------------------------------------
     * get prizes
     * -------------------------------------------------------------------------
     *
     * @access private
     * @return object $this->prizes
     */
    private function getEventPrizes ()
    {
        if($this->eventId)
        {
            /**
                SELECT prz.*, sps.id AS sponsor_id, sps.website AS sponsor_website, sps.name AS sponsor_name
                FROM events_prizes AS prz
                LEFT JOIN sponsors AS sps ON sps.id = prz.sponsor_id
                WHERE event_id = 3;
             */
            $query = $this->db->select('prz.*, sps.id AS sponsor_id, sps.website AS sponsor_website, sps.name AS sponsor_name');
            $query = $this->db->from($this->table_events_prizes.' AS prz');
            $query = $this->db->join($this->table_sponsors.' AS sps', 'sps.id = prz.sponsor_id', 'left');
            $query = $this->db->where('prz.event_id', $this->eventId);

            $query = $this->db->get();

            if ($query->num_rows() > 0)
            {
                $this->prizes = $query->result();
            }
        }
    }


    /**
     * -------------------------------------------------------------------------
     * get sessions for an event
     * -------------------------------------------------------------------------
     *
     * @access public
     * @param boolean $full tells the HTML builder to return a full HTML page
     * @return array $this->eventSession
    **/
    public function getSession ($full = true)
    {
        $presenters = null;
        $strands = null;

        if($this->sessionSlug)
        {
            /**
                SELECT ses.*, sts.name AS status, rms.name AS room_name, evt.hashtag, evt.twitter
                FROM sessions AS ses
                JOIN statuses AS sts ON sts.id = ses.status_id
                JOIN events_sessions AS ess ON ess.session_id = ses.id
                JOIN events AS evt ON evt.id = ess.event_id
                LEFT JOIN rooms AS rms ON rms.id = ses.room_id
                WHERE ses.id = 141
                AND sts.name = 'enabled'
             */
            $query = $this->db->select('ses.*, sts.name AS status, rms.name AS room_name, evt.hashtag, evt.twitter');
            $query = $this->db->from($this->table_sessions.' AS ses');
            $query = $this->db->join($this->table_statuses.' AS sts', 'ses.status_id = sts.id');
            $query = $this->db->join($this->table_events_sessions.' AS ess', 'ess.session_id = ses.id');
            $query = $this->db->join($this->table_events.' AS evt', 'evt.id = ess.event_id');
            $query = $this->db->join($this->table_rooms.' AS rms', 'rms.id = ses.room_id', 'left');
            $query = $this->db->where('ses.id', $this->sessionSlug);
            $query = $this->db->where('sts.name', 'enabled');
            $query = $this->db->get();


            if ($query->num_rows() == 1)
            {
                $row = $query->row();
                $this->eventSession = $row;
                $this->makeSessionHtml($full);
            }
        }
    }


    /**
     * -------------------------------------------------------------------------
     * create the session page html
     * -------------------------------------------------------------------------
     *
     * @access private
     * @param boolean $full tells the HTML builder to return a full HTML page
     * @return string $this->pageHtml
    **/
    private function makeSessionHtml ($full = true)
    {
        $appRoot = project_url();
        
        if($this->sessionSlug AND $this->eventSession)
        {

            if($this->slug)
            {
                $appRoot = project_url().$this->slug.'/';
            }

            if($full)
            {
                $this->pageHtml = '
                    <div class="banner pallet pallet-grey" id="session-title">
                        <div class="container"><h1>'.$this->eventSession->name.'</h1></div><!-- /.container -->
                    </div><!-- /#session-title -->
                ';
            } else {
                $this->pageHtml = '';
            }

            $presenters = $this->makeSessionPresenterCards($appRoot);

            if(strlen($presenters) > 0 OR strlen($this->eventSession->room_name) > 0)
            {
                $room = '';
                if(strlen($this->eventSession->room_name) > 0)
                {
                    $roomUrl = anchor(project_url().$this->slug."/map/", $this->eventSession->room_name, 'target="_blank"');
                    $room = '<h4>'.tbui_icon('map-marker').' '.$roomUrl.'</h4>';
                }

                $this->pageHtml .= '
                    <div class="container presenter-row" id="session-presenters">
                        <div class="row-fluid">
                            <div class="span5">'.$presenters.'</div>
                            <div class="span7">'.$room.'</div>
                        </div>
                    </div><!-- /#session-presenters -->
                ';
            }

            if($full)
            {
                $this->pageHtml .= '
                    <div class="row-fluid">
                        <div class="banner banner-small pallet pallet-plain" id="session-about">
                            <div class="container">
                                <h3>About this session</h3>
                                <p>'.$this->eventSession->description.'</p>
                                <h3>Outcome</h3>
                                <p>'.$this->eventSession->outcome.'</p>
                            </div><!-- /.container -->
                        </div><!-- /#session-about -->
                    </div><!-- /.row-fluid -->
                ';
            } else {
                $this->pageHtml .= '
                    <div class="row-fluid">
                        <div class="banner banner-small pallet pallet-plain" id="session-about">
                            <div class="container-fluid">
                                <h3>About this session</h3>
                                <p>'.$this->eventSession->description.'</p>
                                <h3>Outcome</h3>
                                <p>'.$this->eventSession->outcome.'</p>
                            </div><!-- /.container -->
                        </div><!-- /#session-about -->
                    </div><!-- /.row-fluid -->
                ';
            }
            
            $socialTags = '';
            if(strlen($this->eventSession->hashtag) > 0)
            {
                $socialTags .= '#'.$this->eventSession->hashtag.' ';
            }
            if(strlen($this->eventSession->twitter) > 0)
            {
                $socialTags .= '@'.$this->eventSession->twitter.' ';
            }

            $tweetText = "I'm excited to attend the ".$this->eventSession->name." session ".trim($socialTags);
            $encodedTweetText = urlencode($tweetText);

            $this->pageHtml .= '
                <div class="row-fluid">
                    <div class="banner banner-mini pallet pallet-simple" id="tweet">
                        <div class="container">
                            <div class="row-fluid">
                                <div class="span1">
                                    <a href="https://twitter.com/intent/tweet?text='.$encodedTweetText.'" title="Tweet this!">
                                        <img src="https://dev.twitter.com/sites/default/files/images_documentation/bird_blue_48.png" title="Tweet this!" alt="Tweet this!">
                                    </a>
                                </div>
                                <div class="span11">
                                    <h5>Tweet this Session</h5>
                                    <p>'.$tweetText.'</p>
                                </div>
                            </div>
                        </div><!-- /.container -->
                    </div>
                </div><!-- /.row-fluid -->
            ';
        }
    }


    /**
     * -------------------------------------------------------------------------
     * create presenter cards HTML
     * -------------------------------------------------------------------------
     *
     * @access private
     * @return string of presenter cards HTML
    **/
    private function makeSessionPresenterCards ($appRoot = null)
    {
        $return = '';

        if($this->sessionSlug)
        {
            $presenters = $this->getPresentersBySessionId($this->sessionSlug);

            if($presenters)
            {
                foreach($presenters AS $presenter)
                {
                    $return .= $this->buildPresenterCard($appRoot, $presenter);
                }
            }
        }

        return $return;
    }


    /**
     * -------------------------------------------------------------------------
     * get sessions for an event
     * -------------------------------------------------------------------------
     *
     * @access private
     * @return array $this->eventSessions
    **/
    private function getSessionsByEvent ()
    {
        $presenters = null;
        $strands = null;

        if($this->slug)
        {
            /**
                SELECT ses.*, rms.name AS room_name
                FROM sessions AS ses
                JOIN events_sessions AS ess ON ses.id = ess.session_id
                JOIN events AS evt ON evt.id = ess.event_id
                JOIN statuses AS sts ON ses.status_id = sts.id
                LEFT JOIN rooms AS rms ON rms.id = ses.room_id
                
                -- optional based on submitted user url
                JOIN sessions_presenters AS sps ON sps.session_id = ses.id
                JOIN users AS usr ON usr.id = sps.user_id

                WHERE evt.url_segment = 'isf13'
                AND sts.name = 'enabled'
                AND usr.url = 'rachel-wente-chaney-1'
             */
            $query = $this->db->select('ses.*, rms.name AS room_name');
            $query = $this->db->from($this->table_sessions.' AS ses');
            $query = $this->db->join($this->table_events_sessions.' AS ess', 'ses.id = ess.session_id');
            $query = $this->db->join($this->table_events.' AS evt', 'evt.id = ess.event_id');
            $query = $this->db->join($this->table_statuses.' AS sts', 'ses.status_id = sts.id');
            $query = $this->db->join($this->table_rooms.' AS rms', 'rms.id = ses.room_id', 'left');

            if($this->userSlug)
            {
                $query = $this->db->join($this->table_sessions_presenters.' AS sps', 'sps.session_id = ses.id');
                $query = $this->db->join($this->table_users.' AS usr', 'usr.id = sps.user_id');
                $query = $this->db->where('usr.url', $this->userSlug);
            }

            $query = $this->db->where('evt.url_segment', $this->slug);
            $query = $this->db->where('sts.name', 'enabled');
            $query = $this->db->get();

            if ($query->num_rows() > 0)
            {
                foreach($query->result() AS $row)
                {
                    $this->eventSessions[] = $row;
                }
            }
        }
    }


    /**
     * -------------------------------------------------------------------------
     * generate the attendee 101 and travel page html
     * -------------------------------------------------------------------------
     *
     * @access public
     * @return object $this->pageHtml
    **/
    public function setNavigationByContext ($slug = null, $context = null)
    {
        $return = false;

        if($slug AND $context)
        {
            /**
                SELECT cct.*, sts.name AS status_name
                FROM cms_content AS cct
                JOIN events AS evt ON evt.id = cct.context_id
                JOIN statuses AS sts ON sts.id = cct.status_id
                WHERE cct.context = 'attendee_101'
                AND evt.url_segment = 'isf13'
                AND sts.name = 'enabled'
                ORDER BY cct.ordering asc;
            **/

            $query = $this->db->select('cct.*, sts.name AS status_name');
            $query = $this->db->from($this->table_cms_content.' AS cct');
            $query = $this->db->join($this->table_events.' AS evt', 'evt.id = cct.context_id');
            $query = $this->db->join($this->table_statuses.' AS sts', 'sts.id = cct.status_id');
            $query = $this->db->where('cct.context', $context);
            $query = $this->db->where('evt.url_segment', $slug);
            $query = $this->db->where('sts.name', 'enabled');
            $query = $this->db->order_by('cct.ordering', 'asc');

            $query = $this->db->get();

            if ($query->num_rows() > 0)
            {
                $return = true;
            }
        }

        return $return;
    }


    /**
     * -------------------------------------------------------------------------
     * created the prize page HTML
     * -------------------------------------------------------------------------
     *
     * @access private
     * @return string $this->pageHtml
    **/
    private function makePrizePageHtml ()
    {
        $this->getEventPrizes();

        if($this->prizes)
        {
            foreach($this->prizes AS $prize)
            {
                if(strlen($prize->image) > 0)
                {
                    $prizeImage = project_url().'uploads/images/prizes/'.$prize->image;
                } else {
                    $prizeImage = project_url().'assets/img/no-image-300x300.png';
                }

                $this->pageHtml .= '<div class="row-fluid">';
                $this->pageHtml .= '<div class="banner banner-medium" id="'.$prize->id.'" data-imageId="imageId-'.$prize->id.'">';
                $this->pageHtml .= '<div class="container">';

                    $this->pageHtml .= '<div class="span3">';
                    $this->pageHtml .= '<img src="'.$prizeImage.'" id="imageId-'.$prize->id.'"/>';
                    $this->pageHtml .= '</div><!-- /.span3 -->';

                    $this->pageHtml .= '<div class="span9">';
                    $this->pageHtml .= '<h1>'.$prize->name.'</h1>';
                    
                    if($prize->sponsor_name)
                    {
                        if(strlen($prize->sponsor_website) > 0)
                        {
                            $this->pageHtml .= '<h3 class="muted"><a href="'.$prize->sponsor_website.'" target="_blank">'.$prize->sponsor_name.'</a></h3>';
                        } else {
                            $this->pageHtml .= '<h3 class="muted">'.$prize->sponsor_name.'</h3>';
                        }
                    }

                    $this->pageHtml .= '<p class="lead">'.$prize->description.'</p>';
                    $this->pageHtml .= '</div><!-- /.span9 -->';

                $this->pageHtml .= '</div><!-- /.container -->';
                $this->pageHtml .= '</div><!-- /.banner -->';
                $this->pageHtml .= '</div><!-- /.row-fluid -->';
                $this->pageHtml .= '<hr />';
            }
        }
    }


    /**
     * -------------------------------------------------------------------------
     * created the connect page HTML
     * -------------------------------------------------------------------------
     *
     * @access private
     * @return object $this->pageHtml
    **/
    private function makeConnectPageHtml ()
    {
        if($this->eventId AND $this->sponsorId)
        {
            $this->buildSponsorConnectTable();
            $this->buildSponsorConnectSummary();

            $this->pageHtml  = '<div class="span8">';
            if($this->sponsorConnectTable)
            {
                $this->pageHtml .= $this->sponsorConnectTable;
            }
            $this->pageHtml .= '</div>';

            $this->pageHtml .= '<div class="span4">';
            if($this->sponsorConnectSummary)
            {
                $this->pageHtml .= $this->sponsorConnectSummary;
            }
            $this->pageHtml .= '</div>';

        } else {
            $this->pageHtml = body_message('Test');
        }
    }


    /**
     * -------------------------------------------------------------------------
     * build the event index table
     * -------------------------------------------------------------------------
     *
     * @access public
     * @return string $this->sponsorConnectTable
     */
    public function buildSponsorConnectSummary ()
    {
        $this->countCheckedInRaffleUsersByEvent();

        if($this->eventId AND $this->sponsorId)
        {
            $this->setEventVisitsBySponsorId();
            $this->getSponsor();

            if($this->sponsor)
            {
                // don't divide by zero!
                if($this->checkedInCount > 0)
                {
                    $percentComplete = ($this->eventVisits/$this->checkedInCount)*100;
                    $summaryMessage = '<p>'.$this->eventVisits.' of '.$this->checkedInCount.' attendees have visited you!</p>';
                } else {
                    $percentComplete = 0;
                    $summaryMessage = null;
                }

                if($summaryMessage)
                {
                    $this->sponsorConnectSummary  = '<div id="connection-summary" data-eventUrl="'.$this->slug.'" data-eventId="'.$this->eventId.'" data-sponsorId="'.$this->sponsor->id.'">';
                    $this->sponsorConnectSummary .= '<h3>'.$this->sponsor->name.'</h3>';
                    $this->sponsorConnectSummary .= '<div class="progress progress-info">';
                    $this->sponsorConnectSummary .= '<div class="bar" style="width: '.$percentComplete.'%"></div>';
                    $this->sponsorConnectSummary .= '</div>';
                    $this->sponsorConnectSummary .= $summaryMessage;
                }
                $this->sponsorConnectSummary .= '</div>';
            }
        }
    }


    /**
     * -------------------------------------------------------------------------
     * build the event index table
     * -------------------------------------------------------------------------
     *
     * @access private
     * @return string $this->sponsorConnectTable
     */
    private function buildSponsorConnectTable ()
    {
        $this->getUserVisitDataForEventBySponsor();

        if($this->users)
        {
            $this->load->library('table');
            $this->table->set_template(array ('table_open' => '<table id="data-table-visitor-toggler" class="table table-bordered" data-eventSlug="'.$this->slug.'">'));
            $headings = array('Name', 'Email', 'Visited');
            $this->table->set_heading($headings);

            foreach($this->users AS $user)
            {
                $userName = $user->last.', '.$user->first;
                $email = $user->email;

                $checkedState = '';

                if($user->sponsor_id == $this->sponsorId)
                {
                    $checkedState = 'checked="checked"';
                }

                $checkBox = '
                    <div class="make-switch switch-small visitor-toggler" data-eventId="'.$this->eventId.'" data-sponsorId="'.$this->sponsorId.'" data-userId="'.$user->id.'" data-on-label="<i class=\'icon-ok-sign icon-white\'></i>" data-off-label="<i class=\'icon-minus-sign\'></i>">
                        <input type="checkbox" class="record-visit" id="record-user-'.$user->id.'"'.$checkedState.'>
                    </div>';

                $this->table->add_row(
                    $userName,
                    $email,
                    $checkBox
                );
            }

           $this->sponsorConnectTable = $this->table->generate();
        } else {
            $this->sponsorConnectTable = body_message('No attendees have checked in yet.  Check back soon.');
        }
    }


    /**
     * -------------------------------------------------------------------------
     * gather all the available users and visits for an event by sponsor
     * -------------------------------------------------------------------------
     *
     * @access private
     * @return object $this->users
     */
    private function getUserVisitDataForEventBySponsor ()
    {
        if($this->eventId AND $this->sponsorId)
        {
            /**
                SELECT usr.id, usr.first, usr.last, usr.email, sts.name, evs.sponsor_id
                FROM users AS usr
                JOIN users_events AS uev ON usr.id = uev.user_id
                LEFT JOIN events_visits AS evs ON (usr.id = evs.user_id AND evs.sponsor_id = 22)
                LEFT JOIN users_roles AS url ON url.user_id = usr.id
                LEFT JOIN roles_permissions AS rps ON rps.role_id = url.role_id
                LEFT JOIN permissions AS pms ON pms.id = rps.permission_id
                JOIN statuses AS sts ON usr.status_id = sts.id
                WHERE uev.event_id = 3
                AND uev.arrived IS NOT NULL
                AND sts.name = 'enabled'
                AND pms.code = 'raffle'
                ORDER BY evs.sponsor_id desc;
            **/

            $query = $this->db->select('usr.id, usr.first, usr.last, usr.email, sts.name, evs.sponsor_id');
            $query = $this->db->from($this->table_users.' AS usr');
            $query = $this->db->join($this->table_users_events.' AS uev', 'usr.id = uev.user_id');
            $query = $this->db->join($this->table_events_visits.' AS evs', 'usr.id = evs.user_id AND evs.sponsor_id = '.$this->sponsorId, 'left');

            $query = $this->db->join($this->table_users_roles.' AS url', 'url.user_id = usr.id','left');
            $query = $this->db->join($this->table_roles_permissions.' AS rps', 'rps.role_id = url.role_id', 'left');
            $query = $this->db->join($this->table_permissions.' AS pms', 'pms.id = rps.permission_id', 'left');

            $query = $this->db->join($this->table_statuses.' AS sts', 'usr.status_id = sts.id');
            $query = $this->db->where('uev.event_id', $this->eventId);
            $query = $this->db->where('uev.arrived IS NOT NULL');
            $query = $this->db->where('sts.name', 'enabled');
            $query = $this->db->where('pms.code', 'raffle');
            $query = $this->db->order_by('usr.last', 'asc');
            $query = $this->db->get();

            if ($query->num_rows() > 0)
            {
                $this->users = $query->result();
            }
        }
    }


    /**
     * -------------------------------------------------------------------------
     * gather all the available users and visits for an event by sponsor
     * -------------------------------------------------------------------------
     *
     * @access private
     * @return object $this->checkedInCount
     */
    private function countCheckedInRaffleUsersByEvent ()
    {
        if($this->eventId AND $this->sponsorId)
        {
            /**
                SELECT COUNT(*)
                FROM users AS usr
                JOIN users_events AS uev ON usr.id = uev.user_id
                LEFT JOIN users_roles AS url ON url.user_id = usr.id
                LEFT JOIN roles_permissions AS rps ON rps.role_id = url.role_id
                LEFT JOIN permissions AS pms ON pms.id = rps.permission_id
                JOIN statuses AS sts ON usr.status_id = sts.id
                WHERE uev.event_id = 3
                AND uev.arrived IS NOT NULL
                AND sts.name = 'enabled'
                AND pms.code = 'raffle'
            **/

            $query = $this->db->from($this->table_users.' AS usr');
            $query = $this->db->join($this->table_users_events.' AS uev', 'usr.id = uev.user_id');
            $query = $this->db->join($this->table_users_roles.' AS url', 'url.user_id = usr.id','left');
            $query = $this->db->join($this->table_roles_permissions.' AS rps', 'rps.role_id = url.role_id', 'left');
            $query = $this->db->join($this->table_permissions.' AS pms', 'pms.id = rps.permission_id', 'left');
            $query = $this->db->join($this->table_statuses.' AS sts', 'usr.status_id = sts.id');
            $query = $this->db->where('uev.event_id', $this->eventId);
            $query = $this->db->where('uev.arrived IS NOT NULL');
            $query = $this->db->where('sts.name', 'enabled');
            $query = $this->db->where('pms.code', 'raffle');
            
            $this->checkedInCount = $this->db->count_all_results();
        }
    }


    /**
     * -------------------------------------------------------------------------
     * gather all the available users for an event for a sponsor to connect with
     * -------------------------------------------------------------------------
     *
     * @access private
     * @return object $this->users
     */
    private function setEventVisitsBySponsorId ()
    {
        if($this->eventId AND $this->sponsorId)
        {
            /**
                SELECT COUNT(*) 
                FROM events_visits AS evs
                WHERE event_id = 3
                AND sponsor_id = 22;
            **/

            $query = $this->db->from($this->table_events_visits.' AS evs');
            $query = $this->db->where('event_id',$this->eventId);
            $query = $this->db->where('sponsor_id',$this->sponsorId);
            $this->eventVisits = $this->db->count_all_results();
        }
    }


    /**
     * -------------------------------------------------------------------------
     * gather cms block rows for a certain context like events and event id
     * -------------------------------------------------------------------------
     *
     * @access private
     * @return object $this->pageHtml
    **/
    private function makeBioHtml ()
    {
        if($this->userSlug)
        {
            $this->getUserBySlug();

            if($this->userArray)
            {
                $bioDetails = '';

                $name = $this->userArray->first.' '.$this->userArray->last;

                if(strlen($this->userArray->title) > 0)
                {
                    $title = '<h3 class="bio-title">'.$this->userArray->title.'</h3>';
                } else {
                    $title = '';
                }

                if(strlen($this->userArray->image) > 0)
                {
                    $imageSrc = project_url().'uploads/images/users/'.$this->userArray->image;
                } else {
                    $imageSrc = project_url().'assets/img/no-user-200x200.png';
                }

                $this->pageHtml = '
                    <div class="row-fluid bio-header">
                        <div class="banner pallet pallet-plain">
                            <div class="span3">
                                <img src="'.$imageSrc.'" class="img-circle bio-image">
                            </div>
                            <div class="span9">
                                <h1 class="bio-name">'.$name.'</h1>'.$title.'
                            </div>
                        </div><!-- /#id -->
                    </div><!-- /.row-fluid -->
                ';

                if(strlen($this->userArray->company) > 0)
                {
                    $bioDetails .= '
                        <div class="row-fluid">
                            <div class="span2">
                                <div class="pull-right bio-detail-label">Company</div>
                            </div>
                            <div class="span10" id="bio-company">'.$this->userArray->company.'</div>
                        </div>
                    ';
                }

                if(strlen($this->userArray->bio) > 0)
                {
                    $bioDetails .= '
                        <div class="row-fluid">
                            <div class="span2">
                                <div class="pull-right bio-detail-label">Bio</div>
                            </div>
                            <div class="span10" id="bio-bio">'.$this->userArray->bio.'</div>
                        </div>
                    ';
                }

                if(strlen($this->userArray->twitter) > 0)
                {
                    $twitterLink = '<a href="https://twitter.com/'.$this->userArray->twitter.'" target="_blank">@'.$this->userArray->twitter.'</a>';
                    $bioDetails .= '
                        <div class="row-fluid">
                            <div class="span2">
                                <div class="pull-right bio-detail-label">Twitter</div>
                            </div>
                            <div class="span10" id="bio-twitter">'.$twitterLink.'</div>
                        </div>
                    ';
                }

                if(strlen($this->userArray->facebook) > 0)
                {
                    $fbLink = '<a href="'.$this->userArray->facebook.'" target="_blank">'.$this->userArray->facebook.'</a>';
                    $bioDetails .= '
                        <div class="row-fluid">
                            <div class="span2">
                                <div class="pull-right bio-detail-label">Facebook</div>
                            </div>
                            <div class="span10" id="bio-facebook">'.$fbLink.'</div>
                        </div>
                    ';
                }

                if(strlen($this->userArray->website) > 0)
                {
                    $websiteLink = '<a href="'.$this->userArray->website.'" target="_blank">'.$this->userArray->website.'</a>';
                    $bioDetails .= '
                        <div class="row-fluid">
                            <div class="span2">
                                <div class="pull-right bio-detail-label">Website</div>
                            </div>
                            <div class="span10" id="bio-website">'.$websiteLink.'</div>
                        </div>
                    ';
                }

                if(strlen($bioDetails) > 0)
                {
                    $this->pageHtml .= '<div class="bio-details">';
                    $this->pageHtml .= $bioDetails;
                    $this->pageHtml .= '</div>';   
                }
            }
        }
    }


    /**
     * -------------------------------------------------------------------------
     * retrieves an active user by slug like bio/derek-nutile
     * -------------------------------------------------------------------------
     *
     * @access private
     * @return object $this->user
    **/
    private function getUserBySlug ()
    {
        if($this->userSlug)
        {
            /**
                SELECT usr.*
                FROM users AS usr
                JOIN statuses AS sts ON sts.id = usr.status_id
                WHERE usr.url = 'zac-chase'
                AND sts.name = 'enabled'
            **/
            $query = $this->db->select('usr.*');
            $query = $this->db->from($this->table_users.' AS usr');
            $query = $this->db->join($this->table_statuses.' AS sts', 'sts.id = usr.status_id');
            $query = $this->db->where('url', $this->userSlug);
            $query = $this->db->where('sts.name', 'enabled');
            $query = $this->db->get();

            if ($query->num_rows() == 1)
            {
                $this->userArray = $query->row();
            }
        }
    }


    /**
     * -------------------------------------------------------------------------
     * get sponsor
     * -------------------------------------------------------------------------
     *
     * @access private
     * @return string 
     */
    private function getSponsor ()
    {
        if($this->sponsorId)
        {
            /**
                SELECT * 
                FROM sponsors
                WHERE id = 2;
             */
            $query = $this->db->from($this->table_sponsors);
            $query = $this->db->where('id', $this->sponsorId);
            $query = $this->db->get();

            if ($query->num_rows() == 1)
            {
                $this->sponsor = $query->row();
            }
        }
    }

    /**
     * get settings for the site
     *
     * @access private
     * @return string $this->eventContactEmail
     */
    private function setContactEmailAddress ()
    {
        $this->setEventContactEmailFromSiteDefault();

        if($this->eventId)
        {
            /**
                SELECT * 
                FROM events WHERE id = 2;
             */
            $query = $this->db->from($this->table_events);
            $query = $this->db->where('id', $this->eventId);
            $query = $this->db->get();

            if ($query->num_rows() == 1)
            {
                $row = $query->row();
                $this->eventContactEmail = $row->contact_email;
            }
        }
    }


    /**
     * get settings for the site
     *
     * @access private
     * @return string
     */
    private function setEventContactEmailFromSiteDefault ()
    {
        /**
            SELECT cfg.*
            FROM config_site AS cfg
         */
        $query = $this->db->from($this->table_site_settings);
        $query = $this->db->where('name', 'default-from');
        $query = $this->db->get();

        if ($query->num_rows() == 1)
        {
            $row = $query->row();
            $this->eventContactEmail = $row->value;
        }
    }


    /**
     * -------------------------------------------------------------------------
     * gather cms block rows for a certain context like events and event id
     * -------------------------------------------------------------------------
     *
     * @access private
     * @return object $this->pageHtml
    **/
    private function getContactDetails ()
    {
        if($this->event)
        {
            $this->pageHtml = '';

            if(strlen($this->event[0]->contact_phone) > 0)
            {
                $this->pageHtml .= '<p class="contact-lines">'.tbui_icon("phone-sign").'&nbsp;'.$this->event[0]->contact_phone.'</p>';
            }

            if(strlen($this->event[0]->twitter) > 0)
            {
                $this->pageHtml .= '<p class="contact-lines">'.tbui_icon("twitter-sign").'&nbsp;<a href="https://twitter.com/'.$this->event[0]->twitter.'">@'.$this->event[0]->twitter.'</a></p>';
            }

            if(strlen($this->event[0]->hashtag) > 0)
            {
                $safeHashtag = urlencode('#'.$this->event[0]->hashtag);
                $this->pageHtml .= '<p class="contact-lines">'.tbui_icon("twitter").'&nbsp;<a href="https://twitter.com/search?q='.$safeHashtag.'">#'.$this->event[0]->hashtag.'</a></p>';
            }

            if(strlen($this->event[0]->facebook) > 0)
            {
                $this->pageHtml .= '<p class="contact-lines">'.tbui_icon("facebook-sign").'&nbsp;<a href="'.$this->event[0]->facebook.'">Like Us!</a></p>';
            }
        }
    }


    /**
     * -------------------------------------------------------------------------
     * gather cms block rows for a certain context like events and event id
     * -------------------------------------------------------------------------
     *
     * @access private
     * @return object $this->pageHtml
    **/
    private function buildCmsPageHtml ($context = 'events')
    {
        $this->getCmsContentByContext($context);

        if($this->cmsContent)
        {
            foreach($this->cmsContent AS $content)
            {
                $copy = null;
                $css_class = trim($content->css_class);
                $css_id = trim('banner-'.$content->id);

                if(strlen($content->shortcode_code) > 0)
                {
                    switch ($content->shortcode_code)
                    {
                        case 'shortcodeCountdown':
                            $copy = $this->shortcodeCountdown($content->content);
                            break;

                        case 'shortcodeDescription':
                            $copy = $this->shortcodeDescription();
                            break;

                        case 'shortcodeMap':
                            $copy = $this->shortcodeMap();
                            break;

                        case 'shortcodeMapTall':
                            $copy = $this->shortcodeMap(400);
                            break;

                        case 'shortcodePresenters':
                            $copy = $this->shortcodePresenters(15, 3, 3, false, true);
                            break;

                        case 'shortcodeSponsorCarousel':
                            $copy = $this->shortcodeSponsorCarousel();
                            break;

                        // don't find the shortcode? skip it!
                        default:
                            break;
                    }
                } else {
                    $copy = $content->content;
                }

                if($copy)
                {
                    $this->pageHtml .= $this->wrapCopyInBanner($copy,$css_class,$css_id);
                }
            }
        }
    }


    /**
     * -------------------------------------------------------------------------
     * wrap copy in a container
     * -------------------------------------------------------------------------
     *
     * @access private
     * @param string $copy content to wrap within the banner
     * @param string $class html class=""
     * @param string $id html id=""
     * @return object $this->pageHtml
    **/
    private function wrapCopyInContainer ($copy = null)
    {
        $return = '';

        if($copy)
        {
            $return .= '<div class="container">';
            $return .= $copy;
            $return .= '</div><!-- /.container -->';
        }

        return $return;
    }


    /**
     * -------------------------------------------------------------------------
     * wrap copy in a common banner
     * -------------------------------------------------------------------------
     *
     * @access private
     * @param string $copy content to wrap within the banner
     * @param string $class html class=""
     * @param string $id html id=""
     * @return object $this->pageHtml
    **/
    private function wrapCopyInBanner ($copy = null, $class = '', $id = '')
    {
        $return = '';

        if($copy)
        {
            $return .= '<div class="row-fluid">';
            $return .= '<div class="'.$class.'" id="'.$id.'">';
            // $return .= '<div class="container">';
            $return .= $copy;
            // $return .= '</div><!-- /.container -->';
            $return .= '</div><!-- /#'.$id.' -->';
            $return .= '</div><!-- /.row-fluid -->';
        }

        return $return;
    }


    /**
     * -------------------------------------------------------------------------
     * gather cms block rows for a certain context like events and event id
     * -------------------------------------------------------------------------
     *
     * @access private
     * @return string
    **/
    private function shortcodeCountdown ($content = null)
    {
        if($content)
        {
            $count = $content;
        } else {
            $count = 'Not many';
        }
        $return = '<p class="text-blowout">'.$count.' spots left, hurry up!</p>';
        return $return;
    }


    /**
     * -------------------------------------------------------------------------
     * gather cms block rows for a certain context like events and event id
     * -------------------------------------------------------------------------
     *
     * @access private
     * @return string
    **/
    private function shortcodeDescription ()
    {
        $return = null;

        if($this->eventId)
        {
            /**
                SELECT * FROM events WHERE id = 1
            **/

            $query = $this->db->where('id', $this->eventId);
            $query = $this->db->get($this->table_events);
            
            if ($query->num_rows() == 1)
            {
                $event = $query->row();
                $return = '<p class="text-blowout">'.$event->full_description.'</p>';
            }
        }

        return $return;
    }


    /**
     * -------------------------------------------------------------------------
     * render a google map
     * -------------------------------------------------------------------------
     *
     * @access private
     * @param array $settings
     * @param array $marker
     * @return view
    **/
    private function shortcodeMap ($mapHeight = 200, $marker = null)
    {
        $return = null;
        $this->setMap();

        if($this->mapAddress)
        {
            $settings['center'] = $this->mapAddress;
            // $settings['zoom'] = 'default';
            $settings['map_width'] = '100%';
            $settings['map_height'] = $mapHeight.'px';

            $marker['position'] = $this->mapAddress;
            // $marker['icon'] = 'http://chart.apis.google.com/chart?chst=d_map_pin_letter&chld=F|5b7178|000000';

            $this->load->library('googlemaps');
            $this->googlemaps->initialize($settings);
            $this->googlemaps->add_marker($marker);
            $map = $this->googlemaps->create_map();

            $this->mapJs = $map['js'];
            $return = $map['html'];
        }

        return $return;
    }


    /**
     * -------------------------------------------------------------------------
     * set this->event_id using $this->slug
     * -------------------------------------------------------------------------
     *
     * @access public
     * @return object $this->mapAddress
    **/
    private function setMap ()
    {
        if($this->eventId)
        {
            /**
                SELECT evl.event_id, loc.name, loc.address, loc.city, loc.zipcode, ste.name_long
                FROM events_locations AS evl
                JOIN locations AS loc ON loc.id = evl.location_id
                JOIN states AS ste ON ste.id = loc.state_id
                WHERE evl.event_id = 1
            **/
            $query = $this->db->select('evl.event_id, loc.name, loc.address, loc.city, loc.zipcode, ste.name_long');
            $query = $this->db->from($this->table_events_locations.' AS evl');
            $query = $this->db->join($this->table_locations.' AS loc', 'loc.id = evl.location_id', 'left');
            $query = $this->db->join($this->table_states.' AS ste', 'ste.id = loc.state_id', 'left');
            $query = $this->db->where('evl.event_id', $this->eventId);
            $query = $this->db->get();
            
            if($query->num_rows() == 1)
            {
                $address = $query->row();
                $this->mapAddress = $address->name.' '.$address->address.' '.$address->city.' '.$address->zipcode.' '.$address->name_long;
            }
        }
    }


    /**
     * -------------------------------------------------------------------------
     * render the presenter blocks
     * -------------------------------------------------------------------------
     *
     * @access private
     * @param integer $limit of presenters to query
     * @param integer $perRow is Bootstrap::setGridObjectsPerRow()
     * @param integer $rowMax is Bootstrap::setGridRowsMax()
     * @return string
    **/
    private function shortcodePresenters ($limit = null, $perRow = 3, $rowMax = null, $filter = false, $seeAllLink = false)
    {
        $return = null;
        $this->setPresenters($limit);

        if($this->eventPresenters)
        {
            $presenterCards = array();

            if($this->slug)
            {
                $appRoot = project_url().$this->slug.'/';
            } else {
                $appRoot = project_url();
            }

            $return = '<h2 class="text-center">Presenters</h2>';

            foreach($this->eventPresenters AS $presenter)
            {
                $presenterCard = $this->buildPresenterCard($appRoot, $presenter);

                if($presenterCard)
                {
                    $presenterCards[] = $presenterCard;
                }

            }

            if(count($presenterCards) > 0)
            {
                $this->bootstrap->setGridObjects($presenterCards);
                $this->bootstrap->setGridObjectsPerRow($perRow);
                $this->bootstrap->setGridRowsMax($rowMax);
                $this->bootstrap->buildGrid();

                if($filter)
                {
                    $return  = '
                        <div class="row-fluid" id="find-presenter-form">
                            <div class="span6 offset3">
                                <input type="text" placeholder="Find a Presenter" class="span12 twitter-typeahead" id="filter-presenters"/>
                            </div>
                        </div><!-- /#find-presenter-form -->
                        ';
                }


                $return .= $this->wrapCopyInContainer($this->bootstrap->grid);

                if($seeAllLink == true)
                {
                    $return .= '
                        <h4 class="text-aligned-center"><a href="'.$appRoot.'presenters">See all Presenters</a></h4>
                        ';
                }
            }
        }

        return $return;
    }


    /**
     * -------------------------------------------------------------------------
     * render the sponsor blocks
     * -------------------------------------------------------------------------
     *
     * @access private
     * @param integer $limit of sponsors to query
     * @param integer $perRow is Bootstrap::setGridObjectsPerRow()
     * @param integer $rowMax is Bootstrap::setGridRowsMax()
     * @return string
    **/
    private function shortcodeSponsors ($limit = null, $perRow = 4, $rowMax = null)
    {
        $return = null;
        $this->setSponsors($limit);

        if($this->eventSponsors)
        {
            $sponsorCards = array();

            if($this->slug)
            {
                $appRoot = project_url().$this->slug.'/';
            } else {
                $appRoot = project_url();
            }

            $return = '';

            foreach($this->eventSponsors AS $sponsor)
            {
                $sponsorCard = $this->buildSponsorCard($appRoot, $sponsor);

                if($sponsorCard)
                {
                    $sponsorCards[] = $sponsorCard;
                }

            }

            if(count($sponsorCards) > 0)
            {
                $this->bootstrap->setGridObjects($sponsorCards);
                $this->bootstrap->setGridObjectsPerRow($perRow);
                $this->bootstrap->setGridRowsMax($rowMax);
                $this->bootstrap->buildGrid();

                $return .= $this->bootstrap->grid;
            }
        }

        return $return;
    }


    /**
     * -------------------------------------------------------------------------
     * render the sponsor carousel for an event
     * -------------------------------------------------------------------------
     *
     * @access private
     * @param integer $limit of sponsors to query
     * @param integer $perRow is Bootstrap::setGridObjectsPerRow()
     * @param integer $rowMax is Bootstrap::setGridRowsMax()
     * @return string
    **/
    private function shortcodeSponsorCarousel ($limit = null, $perRow = 4, $rowMax = null)
    {
        $return = '';
        $this->setSponsors($limit);

        if($this->eventSponsors)
        {
            $sponsorCards = array();

            if($this->slug)
            {
                $appRoot = project_url().$this->slug.'/';
            } else {
                $appRoot = project_url();
            }

            foreach($this->eventSponsors AS $sponsor)
            {
                $sponsorCard = $this->buildSponsorCard($appRoot, $sponsor);

                if($sponsorCard)
                {
                    $sponsorCards[] = $sponsorCard;
                }

            }

            if(count($sponsorCards) > 0)
            {
                $this->bootstrap->setGridObjects($sponsorCards);
                $this->bootstrap->setGridObjectsPerRow($perRow);
                $this->bootstrap->setGridRowsMax($rowMax);
                $this->bootstrap->buildCarousel();

                $return = $this->wrapCopyInContainer($this->bootstrap->carousel);
            }
        }

        return $return;
    }


    /**
     * -------------------------------------------------------------------------
     * build the standard presenter card
     * -------------------------------------------------------------------------
     *
     * @access private
     * @param string $appRoot
     * @param object $presenter
     * @return string
    **/
    private function buildPresenterCard ($appRoot = null, $presenter = null)
    {
        $return = null;

        if($presenter AND $appRoot)
        {
            if(isset($presenter->image) AND strlen($presenter->image))
            {
                $image = '<img src="'.project_url().'uploads/images/users/'.$presenter->image.'" class="media-object presenter-thumb">';
            } else {
                $image = '<img src="'.project_url().'assets/img/no-user-200x200.png" class="media-object presenter-thumb">';
            }

            if(isset($presenter->company) AND strlen($presenter->company))
            {
                $company = $presenter->company;
            } else {
                $company = '&nbsp;';
            }

            if(isset($presenter->title) AND strlen($presenter->title))
            {
                $title = $presenter->title;
            } else {
                $title = '&nbsp;';
            }

            $return = '
                <div class="presenter-card">
                    <a href="'.$appRoot.'bio/'.$presenter->url.'">
                        <div class="row-fluid">
                            <div class="span3">'.$image.'</div>
                            <div class="span9">
                                <h6 class="presenter-name">'.$presenter->first_name.' '.$presenter->last_name.'</h6>
                                <p class="presenter-title">'.$title.'</p>
                                <p class="presenter-company">'.$company.'</p>
                            </div>
                        </div>
                    </a>
                </div><!-- /.presenter-card -->
                ';
        }

        return $return;
    }


    /**
     * -------------------------------------------------------------------------
     * build the standard sponsor card
     * -------------------------------------------------------------------------
     *
     * @access private
     * @param string $appRoot
     * @param object $sponsor
     * @return string
    **/
    private function buildSponsorCard ($appRoot = null, $sponsor = null)
    {
        $return = null;

        if($sponsor AND $appRoot)
        {
            if(isset($sponsor->logo) AND strlen($sponsor->logo))
            {
                $logo = '<img src="'.project_url().'uploads/images/sponsors/'.$sponsor->logo.'" class="img-polaroid">';
            } else {
                $logo = '<img src="'.project_url().'assets/img/no-image-300x200.png" class="img-polaroid">';
            }

            $return = '
                <div class="sponsor-card">
                    <a href="'.$sponsor->website.'" target="_blank">'.$logo.'</a>
                </div><!-- /.sponsor-card -->
                ';
        }

        return $return;
    }


    /**
     * -------------------------------------------------------------------------
     * set this->event_id using $this->slug
     * -------------------------------------------------------------------------
     *
     * @access private
     * @return object $this->eventPresenters
    **/
    private function setPresenters ($limit = null)
    {
        if($this->eventId)
        {
            /**
                SELECT usr.id, usr.first AS first_name, usr.last AS last_name, usr.url, usr.company, usr.title, usr.image, usrSts.name
                FROM events_sessions AS ess
                JOIN sessions AS ses ON ses.id = ess.session_id
                JOIN statuses AS sesSts ON sesSts.id = ses.status_id
                JOIN sessions_presenters AS sps ON sps.session_id = ess.session_id
                LEFT JOIN users AS usr ON usr.id = sps.user_id
                JOIN statuses AS usrSts ON usrSts.id = usr.status_id
                WHERE ess.event_id = 1
                GROUP BY usr.id
                ORDER BY usr.last asc
            **/
            $query = $this->db->select('usr.id, usr.first AS first_name, usr.last AS last_name, usr.url,  usr.company, usr.title, usr.image, usrSts.name');
            $query = $this->db->from($this->table_events_sessions.' AS ess');
            $query = $this->db->join($this->table_sessions.' AS ses', 'ses.id = ess.session_id');
            $query = $this->db->join($this->table_statuses.' AS sesSts', 'sesSts.id = ses.status_id');
            $query = $this->db->join($this->table_sessions_presenters.' AS sps', 'sps.session_id = ess.session_id');
            $query = $this->db->join($this->table_users.' AS usr', 'usr.id = sps.user_id', 'left');
            $query = $this->db->join($this->table_statuses.' AS usrSts', 'usrSts.id = usr.status_id');
            $query = $this->db->where('ess.event_id', $this->eventId);
            $query = $this->db->group_by('usr.id');
            $query = $this->db->order_by('usr.last', 'asc');

            if($limit)
            {
                $query = $this->db->limit($limit);
            }

            $query = $this->db->get();

            if ($query->num_rows() > 0)
            {
                $this->eventPresenters = $query->result();
            }
        }
    }


    /**
     * -------------------------------------------------------------------------
     * set this->event_id using $this->slug
     * -------------------------------------------------------------------------
     *
     * @access private
     * @param integer $limit
     * @param boolean $enabled_only
     * @return object $this->eventSponsors
    **/
    private function setSponsors ($limit = null, $enabled_only = true)
    {
        if($this->eventId)
        {
            /**
                SELECT sps.*, sts.name AS status
                FROM sponsors AS sps
                JOIN events_sponsors AS evs ON evs.sponsor_id = sps.id
                JOIN statuses AS sts ON sts.id = sps.status_id
                WHERE evs.event_id = 10
                ORDER BY sps.name asc
            **/
            $query = $this->db->select('sps.*, sts.name AS status');
            $query = $this->db->from($this->table_sponsors.' AS sps');
            $query = $this->db->join($this->table_events_sponsors.' AS evs', 'evs.sponsor_id = sps.id');
            $query = $this->db->join($this->table_statuses.' AS sts', 'sts.id = sps.status_id');
            $query = $this->db->where('evs.event_id', $this->eventId);
            
            if($enabled_only)
            {
                $query = $this->db->where('sts.name', 'enabled');
            }

            $query = $this->db->order_by('sps.name', 'asc');

            if($limit)
            {
                $query = $this->db->limit($limit);
            }

            $query = $this->db->get();

            if ($query->num_rows() > 0)
            {
                $this->eventSponsors = $query->result();
            }
        }
    }


    /**
     * -------------------------------------------------------------------------
     * gather cms content by eventId
     * -------------------------------------------------------------------------
     *
     * @access private
     * @return object $this->cmsContent
    **/
    private function getCmsContentByContext ($context = 'events')
    {
        if($this->eventId)
        {
            /**
                SELECT cct.*, ccs.name AS shortcode_name, ccs.code AS shortcode_code, sts.name AS status_name
                FROM cms_content AS cct
                JOIN statuses AS sts ON sts.id = cct.status_id
                LEFT JOIN cms_content_shortcodes AS ccs ON ccs.id = cct.shortcode_id
                WHERE cct.context = 'events'
                -- AND restricted = 1 -- ties to authentication
                AND cct.context_id = 2
                ORDER BY cct.ordering asc;
            **/

            $query = $this->db->select('cct.*, ccs.name AS shortcode_name, ccs.code AS shortcode_code, sts.name AS status_name');
            $query = $this->db->from($this->table_cms_content.' AS cct');
            $query = $this->db->join($this->table_statuses.' AS sts', 'sts.id = cct.status_id');
            $query = $this->db->join($this->table_cms_content_shortcodes.' AS ccs', 'ccs.id = cct.shortcode_id', 'left');
            $query = $this->db->where('cct.context', $context);
            $query = $this->db->where('cct.context_id', $this->eventId);
            $query = $this->db->where('sts.name', 'enabled');
            $query = $this->db->order_by('cct.ordering', 'asc');
            
            /**
            if($this->authenticated == false)
            {
                $query = $this->db->where('cct.restricted', 0);
            }
            **/

            $query = $this->db->get();

            if ($query->num_rows() > 0)
            {
                $this->cmsContent = $query->result();
            }
        }
    }


    /**
     * -------------------------------------------------------------------------
     * set this->event_id using $this->slug
     * -------------------------------------------------------------------------
     *
     * @access public
     * @return object $this->eventId
    **/
    public function setEventId ()
    {
        if($this->slug)
        {
            /**
                SELECT * FROM events WHERE url_segment = 'isf'
            **/
            $query = $this->db->from($this->table_events);
            $query = $this->db->where('url_segment', $this->slug);
            $query = $this->db->get();
            
            $event = $query->row();
            $this->eventId =  $event->id;
        }
    }


    /**
     * get all strands for an event
     *
     * @access private
     * @param string $class adds a filter for class
     * @return object $this->strands
     */
    private function getStrands ($class = null)
    {
        $this->strands = null;

        if($this->eventId)
        {
            /**
                SELECT sds.*, est.event_id
                FROM strands AS sds
                JOIN events_strands AS est ON est.strand_id = sds.id
                WHERE est.event_id = 1
                AND sds.class = 'blue'
             */
            $query = $this->db->select('sds.*, est.event_id');
            $query = $this->db->from($this->table_strands.' AS sds');
            $query = $this->db->join($this->table_events_strands.' AS est', 'est.strand_id = sds.id');
            $query = $this->db->where('est.event_id', $this->eventId);

            if($class)
            {
                $query = $this->db->where('sds.class', $class);
            }

            $query = $this->db->get();

            if ($query->num_rows() > 0)
            {
                $this->strands = $query->result();
            }
        }
    }
}

/* End of eventrouter model */
/* Pocation: application/webadmin/modules/eventrouter/models/eventrouter_model.php */
