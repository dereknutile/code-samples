# Code Samples

## Overview
This is a small repository with a cross section of different code samples.

## How to Review
You could either browse the source using [BitBucket Source](https://bitbucket.org/dereknutile/code-samples/src), or download the collection in a [zip file](https://bitbucket.org/dereknutile/code-samples/get/master.zip) and review locally using your favorite editor.