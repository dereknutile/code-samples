-- MSSQL product match
-- Query to pull all products that match Etilize products adding details
-- like description, manufacturer, category, etc.

SELECT
    oep.store,
    oep.websites,
    oep.attribute_set,
    oep.type,
    TRIM(oep.MNFCTRITMNMBR) AS sku,
    ec.categoryid AS category_ids,
    oep.has_options,
    TRIM(oep.ITEMDESC) AS name,
    oep.meta_title,
    oep.meta_description,
    REPLACE(LOWER(TRIM(oep.ITEMDESC))," ", "-") AS url_key,
    REPLACE(LOWER(TRIM(oep.ITEMDESC))," ", "-") AS url_path,
    oep.options_container,
    oep.groupscatalog_hide_group,
    oep.LISTPRCE AS price,
    oep.ITEMSHWT AS weight,
    TRIM(pd1.description) AS description, 
    TRIM(pd3.description) AS short_description, 
    oep.status,
    oep.tax_class_id,
    oep.visibility,
    oep.enable_googlecheckout,
    oep.manage_stock,
    oep.use_config_manage_stock,
    TRIM(oep.ITEMDESC) AS product_name,
    oep.store_id,
    oep.product_type_id,
    em.name AS manufacturer    
FROM
    oetcproducts oep
JOIN oetc_etilize_junction oej
    ON oep.USCATVLS_1 = oej.manufacturername
JOIN product ep 
    ON ep.manufacturerid= oej.manufacturerid
    AND ep.mfgpartno = oep.MNFCTRITMNMBR 
JOIN categorynames ec 
    ON ep.categoryid = ec.categoryid
    AND ec.localeid=1
JOIN manufacturer em 
    ON ep.manufacturerid = em.manufacturerid
LEFT JOIN productdescriptions pd1 
    ON ep.productid=pd1.productid 
    AND pd1.type=1 
    AND pd1.localeid=1 
LEFT JOIN productdescriptions pd2 
    ON ep.productid=pd2.productid 
    AND pd2.type=2 
    AND pd2.localeid=1 
LEFT JOIN productdescriptions pd3 
    ON ep.productid=pd3.productid 
    AND pd3.type=3 
    AND pd3.localeid=1
;